package kk.practicum.code.hexlet.xo;

public class Board {

    private static final String GAME_NAME = "XO";

    private Figure cell11 = new Figure("1-1", " ");
    private Figure cell12 = new Figure("1-2", " ");
    private Figure cell13 = new Figure("1-3", " ");

    private Figure cell21 = new Figure("2-1", " ");
    private Figure cell22 = new Figure("2-2", " ");
    private Figure cell23 = new Figure("2-3", " ");

    private Figure cell31 = new Figure("3-1", " ");
    private Figure cell32 = new Figure("3-2", " ");
    private Figure cell33 = new Figure("3-3", " ");

//    Figure[][] board = {{cell11, cell12, cell13}, {cell21, cell22, cell23}, {cell31, cell32, cell33}};


    public void printBoard() {
        System.out.printf("%s|%s|%s \n", cell11.getCellInfo(), cell12.getCellInfo(), cell13.getCellInfo());
        System.out.println("-----");
        System.out.printf("%s|%s|%s \n", cell21.getCellInfo(), cell22.getCellInfo(), cell23.getCellInfo());
        System.out.println("-----");
        System.out.printf("%s|%s|%s \n", cell31.getCellInfo(), cell32.getCellInfo(), cell33.getCellInfo());

    }

    public static void printGameName() {
        System.out.println("Game - " + Board.GAME_NAME);
    }
}
