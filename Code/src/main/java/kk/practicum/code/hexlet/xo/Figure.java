package kk.practicum.code.hexlet.xo;

public class Figure {

    protected String cellPosition;
    protected String cellInfo;

    public Figure(String position, String cell) {
        this.cellPosition = position;
        this.cellInfo = cell;
    }

    public String getCellInfo() {
        return cellInfo;
    }

    public void setCellInfo(String cellInfo) {
        this.cellInfo = cellInfo;
    }

    @Override
    public String toString() {
        return "Figure{" +
                "position='" + cellPosition + '\'' +
                ", cell='" + cellInfo + '\'' +
                '}';
    }
}
