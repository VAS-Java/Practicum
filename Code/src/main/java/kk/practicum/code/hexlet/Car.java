package kk.practicum.code.hexlet;

public class Car {

    int maxSpeed;
    int currentSpeed;
    String fuelType;

    int passengerCount;


    public void startEngine() {
        System.out.println("Engine started.");
    }
    public void stopEngine() {
        System.out.println("Engine stop.");
    }
    public void setSpeed(int newSpeed) {
        this.currentSpeed = newSpeed;
    }
    public void showCurrentSpeed() {System.out.println(this.currentSpeed + " Km/h.");}

}