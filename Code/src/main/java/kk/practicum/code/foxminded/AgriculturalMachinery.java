package kk.practicum.code.foxminded;

import java.util.Objects;

public abstract class AgriculturalMachinery implements Serviceable {

	String conсern;
	String mark;
	String modelName;
	Color color;
	int yearOfProduction;
	int price;
	int weight;
	private double distance = 0;
	protected int distanceOnService = 0;

	public AgriculturalMachinery(String conсern, String mark, String modelName, Color color, int yearOfProduction,
			int price, int weight) {
		this.conсern = conсern;
		this.mark = mark;
		this.modelName = modelName;
		this.color = color;
		this.yearOfProduction = yearOfProduction;
		this.price = price;
		this.weight = weight;
	}

	public void addDistance(int additionalDistance) {
		if (additionalDistance < 1) {
			System.out.println("Введите значение больше 1");
		} else {
			distance += additionalDistance;
			distanceOnService += additionalDistance;
		}
	}

	public void addDistance(double additionalDistance) {
		if (additionalDistance < 1.0) {
			System.out.println("Введите значение больше 1");
		} else {
			distance += additionalDistance;
			distanceOnService += additionalDistance;
		}
	}

	public int getDistanceOnService() {
		return distanceOnService;
	}

	public double getDistance() {
		return distance;
	}

	@Override
	public String toString() {
		return "AgriculturalMachinery [conсern=" + conсern + ", mark=" + mark + ", modelName=" + modelName + ", color="
				+ color + ", yearOfProduction=" + yearOfProduction + ", price=" + price + ", weight=" + weight
				+ ", distance=" + distance + ", distanceOnService=" + distanceOnService + "]";

	}

	@Override
	public int hashCode() {
		return Objects.hash(color, conсern, distance, distanceOnService, mark, modelName, price, weight,
				yearOfProduction);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgriculturalMachinery other = (AgriculturalMachinery) obj;
		return color == other.color && Objects.equals(conсern, other.conсern)
				&& Double.doubleToLongBits(distance) == Double.doubleToLongBits(other.distance)
				&& distanceOnService == other.distanceOnService && Objects.equals(mark, other.mark)
				&& Objects.equals(modelName, other.modelName) && price == other.price && weight == other.weight
				&& yearOfProduction == other.yearOfProduction;
	}

}
