package kk.practicum.code.foxminded;

import java.util.ArrayList;
import java.util.List;

public class CarManager {

	public static void main(String[] args) {

		Car rapid = new PassengerCar("VAG", "Skoda", "Rapid", Color.WHITE, 2017, 700_000, 1_300);
		Car largus = new PassengerCar("Renault & Nissan", "VAZ", "largus", Color.RED, 2019, 700_000, 1_500);
		Car largusWhite = new PassengerCar("Renault & Nissan", "VAZ", "largus", Color.RED, 2019, 700_000, 1_500);

		Car honda = new PassengerCar("Honda Inc", "Honda", "Civic", Color.ORANGE, 2010, 550_000, 1_300);

		largusWhite.addDistance(1500);
		largusWhite.addDistance(15.5);
		largusWhite.addDistance(-1500);

		System.out.println("\n" + rapid + "\n" + largus + "\n" + largusWhite);
		System.out.println(honda);

		if (largusWhite.equals(largus)) {
			System.out.println(largusWhite + " is " + largus);
		} else {
			System.out.println(largusWhite + " not is " + largus);
		}
		if (largusWhite.equals(honda)) {
			System.out.println(largusWhite + " is " + honda);
		} else {
			System.out.println(largusWhite + " not is " + honda);
		}

		Car mersedesSprinter = new Bus("Mersedes Inc", "Mersedes", "Sprinte", Color.GREY, 2020, 5_000_000, 4_500);
		Car oktavia = new PassengerCar("VAG", "Skoda", "Oktavia", Color.GREY, 2010, 500_000, 1_500);
		System.out.println(mersedesSprinter + "\n" + oktavia);
		System.out.println(mersedesSprinter.isReadyToService());
		System.out.println(oktavia.isReadyToService());
		mersedesSprinter.addDistance(5000);
		oktavia.addDistance(15000);
		System.out.println(mersedesSprinter + "\n" + oktavia);
		System.out.println(mersedesSprinter.isReadyToService());
		System.out.println(oktavia.isReadyToService());
		
		
		List<Car> cars = new ArrayList<>();
		cars.add(rapid);
		cars.add(largus);
		cars.add(largusWhite);
		cars.add(honda);
		cars.add(mersedesSprinter);
		cars.add(oktavia);
		System.out.println(cars);

	}
}
