package kk.practicum.code.foxminded;

public enum Color {

	WHITE("Белый"), RED("Красный"), BLACK("Черный"), BLUE("Синий"), GREEN("Зеленый"), GREY("Серый"),
	SILVER("Серебрянный"), ORANGE("Оранжевый");

	private String title;

	Color(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return title;
	}
 
}
