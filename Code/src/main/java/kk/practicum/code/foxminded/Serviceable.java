package kk.practicum.code.foxminded;

public interface Serviceable {
	
	public boolean isReadyToService();
	
	public double getDistance();
	
	public int getDistanceOnService();
	
	public void addDistance(int additionalDistance);
	
	public void addDistance(double additionalDistance);
	

}
