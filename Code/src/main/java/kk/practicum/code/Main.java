package kk.practicum.code;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Main {

    public static void main(String[] args) {
        LocalDateTime today = LocalDateTime.now();
        LocalTime durationInit = LocalTime.parse("05:10");
        LocalTime additionalDuration = LocalTime.parse("10:15");
        System.out.println("Hello world!");
        System.out.println(durationInit);
        durationInit = durationInit.plus(additionalDuration.getHour(), ChronoUnit.HOURS).plus(additionalDuration.getMinute(), ChronoUnit.MINUTES);
        System.out.println(durationInit);

    }


}