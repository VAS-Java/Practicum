package kk.practicum.code.theory;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class Time {


    public static void main(String[] args) {

        Instant instantTimeStamp = Instant.now();
        System.out.println(instantTimeStamp);

        LocalDateTime localTimeStamp = LocalDateTime.now();
        System.out.println(localTimeStamp);
        LocalDateTime myBirthday = LocalDateTime.of(1987, 2, 3, 12, 0);
        System.out.println(myBirthday);
        LocalDateTime initDelivery = LocalDateTime.of(2025, 6, 1, 8, 30);

        Duration iak = Duration.between(myBirthday, LocalDateTime.of(1987,3,23,12,0));
        System.out.println(iak.getSeconds());

        DeliveryShift(initDelivery, 8, 6);
        DeliveryShift(initDelivery, 12, 6);
        timeDecoding();
        igorCode();
        yearsOld();



    }


    public static LocalDateTime timeDecoding() {
        System.out.println("\n API time decoding. \n");
        String time = "07 часов 09 минут. Месяц: 06, День: 09, Год:2018";
        DateTimeFormatter formater = DateTimeFormatter.ofPattern("HH часов mm минут. Месяц: MM, День: dd, Год:yyyy");
        LocalDateTime locTime = LocalDateTime.parse(time, formater);

        System.out.println(locTime);
        System.out.println(locTime.format(DateTimeFormatter.ofPattern("dd_MM_yy|HH_mm")));
        return locTime;
    }


    public static void DeliveryShift(LocalDateTime start, int hoursIn, int shiftIn) {
        System.out.println("\n Delivery Shift \n");
        if (hoursIn > 8) {
            System.out.println("\nThe shift have to 8 hours maximum.");
            return;
        }
        LocalDateTime initDelivery = start;

        for (int i = 1; i < shiftIn + 1; i++) {
            System.out.printf("\n%d смена. Начало: %s, конец: %s.".formatted(i, initDelivery, initDelivery.plusHours(hoursIn)));
            initDelivery = initDelivery.plusHours(hoursIn);
        }
    }

    public static void igorCode() {
        System.out.println("\n IGOR Code.\n");
        LocalDate inDate = LocalDate.of(2020, 1, 10);
        LocalTime inTime = LocalTime.of(12, 30);
        LocalDateTime inDT = LocalDateTime.of(inDate, inTime);
        inDT = inDT.minusMonths(2).minusDays(25).minusMinutes(10);
        System.out.println(inDT.getDayOfYear() * inDT.getHour());

    }

    public static void yearsOld() {
        LocalDate myBirthday = LocalDate.of(1987,2,3);
        System.out.println("\n OLD");
        System.out.printf("\nYear's - %s".formatted(Period.between(myBirthday,LocalDate.now()).getYears()));
        System.out.printf("\nMonth's - %d".formatted(Period.between(myBirthday,LocalDate.now()).getMonths()));;
        System.out.printf("\nDay's - %d".formatted(Period.between(myBirthday,LocalDate.now()).getDays()));;
    }


}
