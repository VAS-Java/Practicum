package kk.practicum.code.theory.http.basic.server;

public enum Entertainments {

    RUN,
    SLEEP,
    PLAY,
    PLAY_WITH_TOI,
    EAT,
    SWIM,
    GUARD,
    JUMP,
    HAVE_FUN,
    WALK_ON_RUFF,


    NULL
}
