package kk.practicum.code.theory.test;

public class DiscountCalculator {


    public static void main(String[] args) {

        DiscountCalculator calc = new DiscountCalculator();
        System.out.println(calc.subAfterDiscount(1500));

    }



    public int subAfterDiscount(int sum) {

        if (sum < 1000) {
            return sum;
        } else {
            return (int) (sum * 0.98);
        }
    }


}


