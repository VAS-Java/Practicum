package kk.practicum.code.theory.http.uri;

import com.google.gson.*;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class CurrencyLayer {

    public static void main(String[] args) {

//        URI uri = URI.create("https://currencylayer.com/api/live");
        URI uri = URI.create("http://apilayer.net/api/live?access_key=bf5078b1ab3ffbcc47b7b508f42d3bcf&currencies=EUR,%20RUB,%20UAH&source=USD&format=1");

        HttpRequest request = HttpRequest.newBuilder().
                GET().
                uri(uri).
                build();

        HttpClient client = HttpClient.newHttpClient();

        HttpResponse.BodyHandler<String> handler = HttpResponse.BodyHandlers.ofString();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        try {
            HttpResponse<String> response = client.send(request, handler);
            JsonElement jsonElement = JsonParser.parseString(response.body());
            String [] splitLayer = gson.toJson(jsonElement).split(",");
            String eur = splitLayer[5].replace("{", "---").split("---")[1];
            String rub = splitLayer[6];
            String uah = splitLayer[7].replace("}", "");
            System.out.println(eur + rub + uah);

        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }


    }


}
