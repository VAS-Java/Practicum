package kk.practicum.code.theory;

import java.io.File;
import java.io.IOException;

public class CreateFile {

    public static void main(String[] args) {

//        File targetFile = new File("/home/vas/Documents/Java_WorkSpace/Practicum/Code/" +
//                "src/main/java/kk/practicum/code/yandex/ft/tasktracker/data", "dataTracker.csv");
        File targetFile = new File("Practicum/Code/src/main/java/kk/practicum/code/yandex/ft/tasktracker/data", "dataTracker.csv");
        try {
            targetFile.createNewFile();
        } catch (IOException e) {
            targetFile.delete();
            System.out.println(e.fillInStackTrace());
        }
        targetFile.delete();
    }

}
