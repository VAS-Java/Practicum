package kk.practicum.code.theory;

public class StringOptions {


    public static void main(String[] args) {
        cardCode("1234 5678 8765 4321");
        listEvaluation("Анна Андреева, Виталий Кудлай, Анастасия Миронова, Ольга Егорова, Анна Ромаз");
    }


    public static String cardCode(String card) {
        System.out.println(card.substring(0, 4) + " " +
                card.substring(5, 9).replace("5678", "*VAS*") + " " +
                card.substring(10, 14).replace("8765", "*VAS*") + " " +
                card.substring(card.length() - 4));
        return card.substring(0, 4) + " " +
                card.substring(5, 9).replace("5678", "*VAS*") + " " +
                card.substring(10, 14).replace("8765", "*VAS*") + " " +
                card.substring(card.length() - 4);
    }

    public static void listEvaluation(String names) {
        String[] lstNames = names.split(",");
        for (int i = 0; i < lstNames.length; i++) {
            System.out.printf("%30s - Good. \n", lstNames[i].trim());
            System.out.printf("%-30s - Good. \n", lstNames[i].trim());
        }
    }

}





