package kk.practicum.code.theory.http.uri;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Ipwhois {

    public static void main(String[] args) {

        URI uri = URI.create("https://ipwhois.app/json/46.226.227.20?lang=ru");

        HttpRequest request = HttpRequest.newBuilder().
                GET().
                uri(uri).
                build();

        HttpClient client = HttpClient.newHttpClient();

        HttpResponse.BodyHandler<String> handler = HttpResponse.BodyHandlers.ofString();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        try {
            HttpResponse<String> response = client.send(request, handler);
            System.out.println(response.statusCode());
            System.out.println(response.body());
            JsonElement jsonElement = JsonParser.parseString(response.body());
            System.out.println(jsonElement);
            System.out.println(gson.toJson(jsonElement));
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }


    }




}
