package kk.practicum.code.theory;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class TimeClear {


    public static void main(String[] args) {

        LocalDateTime initDTPosition = LocalDateTime.now();
        System.out.println(initDTPosition.toLocalTime());
        LocalTime initTime = LocalTime.of(15, 15);
        System.out.println(initTime);


        LocalDateTime updDTPosition = initDTPosition.minus(3, ChronoUnit.HOURS);
        System.out.println(updDTPosition.toLocalTime());
        updDTPosition = updDTPosition.plus(initTime.getHour(),ChronoUnit.HOURS).plus(initTime.getMinute(),ChronoUnit.MINUTES);
        System.out.println(updDTPosition.toLocalTime());





    }


}
