package kk.practicum.code.theory.http.basic.server;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class AnimalInfo implements HttpHandler {


    public List<Animal> generator() {
        Owner KSY = new Owner("Vysochina", "Kseniy");

        Animal GERDA = new Animal(KSY, "Gerda", "Black & Tan", "Dog", "Doberman",
                new ArrayList<Entertainments>(Arrays.asList(new Entertainments[]{Entertainments.RUN, Entertainments.GUARD})),
                Gender.FEMALE);

        Animal KOTE = new Animal(KSY, "LeLic", "Black", "Cat", "Normal",
                new ArrayList<Entertainments>(Arrays.asList(new Entertainments[]{Entertainments.SLEEP, Entertainments.EAT, Entertainments.WALK_ON_RUFF})),
                Gender.MAN);

        List<Animal> animals = new ArrayList<>(Arrays.asList(new Animal[]{GERDA, KOTE}));

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();

        try {
            BufferedReader jsonAnimal = new BufferedReader(new FileReader("src/main/java/kk/practicum/code/theory/http/json/animalInfo.json"));
            Animal importAnimal = gson.fromJson(jsonAnimal, Animal.class);
            animals.add(importAnimal);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return animals;
    }


    @Override
    public void handle(HttpExchange httpExchange) throws IOException {

        httpExchange.sendResponseHeaders(200, 0);

        GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting().serializeNulls();
        Gson gson = gsonBuilder.create();

        try (OutputStream os = httpExchange.getResponseBody()) {
            os.write(" --VAS-- \n".getBytes(StandardCharsets.UTF_8));
            if (Objects.equals(httpExchange.getRequestMethod(), "GET")) {
                for (Animal animal : generator()) {
                    os.write("%s\n".formatted(gson.toJson(animal)).getBytes(StandardCharsets.UTF_8));
                }
            } else if (Objects.equals(httpExchange.getRequestMethod(), "POST")) {
                Headers httpHead = httpExchange.getRequestHeaders();
                String[] splitURI = httpExchange.getRequestURI().getPath().split("/");
                if ((httpHead.containsKey("X-Wish-Good-Day"))
                        && (httpHead.get("X-Wish-Good-Day").getFirst().equals("true"))) {
                    os.write("Good day %s   %s".formatted(splitURI[2], splitURI[3]).getBytes(StandardCharsets.UTF_8));
                } else if ((httpHead.containsKey("X-Wish-Good-Day"))
                        && (httpHead.get("X-Wish-Good-Day").getFirst().equals("false"))) {
                    os.write("Hello %s   %s".formatted(splitURI[2], splitURI[3]).getBytes(StandardCharsets.UTF_8));
                } else if ((httpHead.containsKey("X-Import-Animals"))
                        && (httpHead.get("X-Import-Animals").getFirst().equals("true"))) {
                    BufferedReader jsonAnimals = new BufferedReader(
                            new FileReader("src/main/java/kk/practicum/code/theory/http/json/animalsInfo.json"));
                    JsonElement jsonElement = JsonParser.parseReader(jsonAnimals);
                    os.write("%s".formatted(jsonElement).getBytes(StandardCharsets.UTF_8));
                } else if ((httpHead.containsKey("X-Import-Animals"))
                        && (httpHead.get("X-Import-Animals").getFirst().equals("false"))) {
                    BufferedReader jsonAddress = new BufferedReader(
                            new FileReader("src/main/java/kk/practicum/code/theory/http/json/address.json"));
                    JsonElement jsonElement = JsonParser.parseReader(jsonAddress);
                    os.write("%s".formatted(jsonElement).getBytes(StandardCharsets.UTF_8));
                }

            } else
                os.write("Incorrect method - %s".formatted(httpExchange.getRequestMethod()).getBytes(StandardCharsets.UTF_8));

        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
