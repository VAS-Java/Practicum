package kk.practicum.code.theory.http.uri;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class LocalHost {

    public class HttpStatuses {

        public static void main(String[] args) {

            URI uri = URI.create("http://127.0.0.1:5050/info/Vetrinary/Employee");
            HttpRequest.Builder requestBilder = HttpRequest.newBuilder();

            HttpRequest request = HttpRequest.newBuilder().
                    GET().
                    uri(uri).
                    version(HttpClient.Version.HTTP_1_1).
                    header("X-Wish-Good-Day", "true").
                    build();

            HttpClient client = HttpClient.newHttpClient();

            HttpResponse.BodyHandler<String> handler = HttpResponse.BodyHandlers.ofString();

            try {
                HttpResponse<String> response = client.send(request, handler);
                System.out.println(response.statusCode());
                System.out.println(response.body());
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }


        }


    }

}
