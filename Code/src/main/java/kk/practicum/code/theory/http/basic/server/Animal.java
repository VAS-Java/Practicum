package kk.practicum.code.theory.http.basic.server;

import java.util.List;

public class Animal {

    private int id;
    private static int idNumbers;
    private Owner owner;
    private String name;
    private String animalSpecies;
    private String breed;
    private String color;

    private Gender gender;
    private List<Entertainments> entertainments;

    public Animal(Owner owner, String name, String color, String animalSpecies, String breed, List<Entertainments> entertainments, Gender gender) {

        this.owner = owner;
        this.name = name;
        this.animalSpecies = animalSpecies;
        this.breed = breed;
        this.color = color;
        this.gender = gender;
        this.entertainments = entertainments;

        this.id = idNumbers;
        idNumbers++;

    }


    public String getAnimalSpecies() {
        return animalSpecies;
    }

    public String getBreed() {
        return breed;
    }

    public String getColor() {
        return color;
    }

    public List<Entertainments> getEntertainments() {
        return entertainments;
    }

    public Gender getGender() {
        return gender;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Owner getOwner() {
        return owner;
    }


    @Override
    public String toString() {
        return "Animal{" +
                "id=" + id +
                ", owner=" + owner +
                ", name='" + name + '\'' +
                ", animalSpecies='" + animalSpecies + '\'' +
                ", breed='" + breed + '\'' +
                ", color='" + color + '\'' +
                ", gender=" + gender +
                ", entertainments=" + entertainments +
                '}';
    }
}

