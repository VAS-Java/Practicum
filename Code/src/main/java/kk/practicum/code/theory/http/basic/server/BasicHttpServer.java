package kk.practicum.code.theory.http.basic.server;


import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

public class BasicHttpServer {
    private static final int PORT = 5050;


    public static void main(String[] args) {

        try {

            HttpServer newServer = HttpServer.create();
            newServer.bind(new InetSocketAddress(PORT), 0);
            newServer.createContext("/info", new AnimalInfo());
            newServer.start();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
