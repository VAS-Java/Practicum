package kk.practicum.code.theory.test;


import org.junit.Assert;
import org.junit.Test;

public class DiscountCalculatorTest {

    DiscountCalculator calc = new DiscountCalculator();


@Test
    public void shouldGiveNoDiscountValue999() {
        int bySum = 999;
        int expectedSum = 999;
        int realSum = calc.subAfterDiscount(bySum);
        Assert.assertEquals(realSum, expectedSum);
    }

}