package kk.practicum.code.theory.http.basic.server;

import java.util.List;

public class Owner {

    private int id;
    private static int idNumbers = 0;
    private String name;
    private String family;
    private List<Animal> animals;

    public Owner(String family, String name) {
        this.family = family;
        this.id = idNumbers;
        this.name = name;
        idNumbers++;
    }

    public String getFamily() {
        return family;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }
}
