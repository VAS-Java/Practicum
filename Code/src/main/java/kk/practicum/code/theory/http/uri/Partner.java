package kk.practicum.code.theory.http.uri;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Partner {


    public static void main(String[] args) {

        URI uri = URI.create("https://api.partner.market.yandex.ru/v2/regions");

        HttpRequest request = HttpRequest.newBuilder().
                GET().
                uri(uri).
                header("Accept", "applications/json").
                build();

        HttpClient client = HttpClient.newHttpClient();

        HttpResponse.BodyHandler<String> handler = HttpResponse.BodyHandlers.ofString();

        try {
            HttpResponse<String> response = client.send(request, handler);
            System.out.println(response.statusCode());
            System.out.println(response.body());
            JsonElement jsonElement = JsonParser.parseString(response.body());
            System.out.println(jsonElement);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }


    }



}
