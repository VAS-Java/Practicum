package kk.practicum.code.theory;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class ReadFile {

    public static void main(String[] args) {

        String HOME = "Practicum/Code/src/main/java/kk/practicum/code/yandex/ft/tasktracker/data/";
        String line;

        try {
            BufferedReader br = new BufferedReader(new FileReader(HOME + "saveData.csv"));
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                String[] lineUPD = line.split(",");
                if (lineUPD[4].equals("null")) {
                    if (lineUPD[2].equals("Epic")) {
                        new Epic(lineUPD[2], lineUPD[3]);
                    } else {
                        new SubTask(lineUPD[2], lineUPD[3]);
                    }
                }else {

                }

            }

        } catch (IOException e) {
            System.out.println(e);
        }


    }


}
