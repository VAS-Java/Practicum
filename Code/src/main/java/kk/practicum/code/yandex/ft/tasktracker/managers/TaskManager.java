package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Status;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.util.HashMap;

public interface TaskManager {

    void showTasks();
    void detailInformation();

    HashMap<Integer, Task> getAllTasks();
    Task getTask(int taskID);
    Epic getEpic(int epicID);
    SubTask getSubTask(int subTaskID);

    void exit();

    void clearTracker();
    void deleteAllEpics();
    void deleteAllTasks();
    void deleteTaskOrEpic();
    void deleteSubTask();

    void createTask(Task task);
    void createEpic(Epic epic);
    void createSubTask(SubTask subTask);
    void updateTask();
    void updateEpic();
    void updateSubTask();

    void updateStatus(int id, Status status);



}

