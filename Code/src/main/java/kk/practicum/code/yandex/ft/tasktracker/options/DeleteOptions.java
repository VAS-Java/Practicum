package kk.practicum.code.yandex.ft.tasktracker.options;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;
import kk.practicum.code.yandex.ft.tasktracker.managers.HistoryManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DeleteOptions {

    HashMap<Integer, Task> allTasks;
    HashMap<Integer, Integer> subEpic;
    HistoryManager history;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public DeleteOptions(HashMap<Integer, Task> allTasks,HashMap<Integer, Integer> subEpic, HistoryManager history) {
        this.allTasks = allTasks;
        this.subEpic = subEpic;
        this.history = history;

    }

    public void clearTaskTracker() {
        allTasks.clear();
        history.clear();
    }

    public void deleteAllEpics() {
        ArrayList<Integer> dellEpics = new ArrayList<>();
        for (Task item : allTasks.values()) {
            if (item instanceof Epic) {
                dellEpics.add(item.getTaskId());
            }
        }
        for (Integer i : dellEpics) {
            allTasks.remove(i);
            history.remote(i);
        }
    }


    public void deleteAllTasks() {
        List<Integer> dellSub = new ArrayList<>();
        for (Task item : allTasks.values()) {
            if (item instanceof SubTask) {
                dellSub.add(item.getTaskId());
            }
        }
        for (Integer i : dellSub) {
            allTasks.remove(i);
            history.remote(i);
        }
    }

    public void deleteTaskOrEpic() {
        try {
            System.out.println("Input - ID Task or Epic.");
            String taskSTR = reader.readLine();
            allTasks.remove(Integer.parseInt(taskSTR));
            history.remote(Integer.parseInt(taskSTR));
        } catch (Exception e) {
            System.out.println("Unpredictable ID in input.");
        }

    }

    public void deleteMenu() {
        System.out.println(" Pls 1 for - Delete all Task's.");
        System.out.println(" Pls 2 for - Delete all SubTask's.");
        System.out.println(" Pls 3 for - Delete all Epic's.");
        System.out.println(" Pls 4 for - Delete Task on ID.");
        System.out.println(" Pls 9 for - Main menu.");
    }

    public void deleteIDMenu() {
        System.out.println(" Pls 1 for - Delete Task.");
        System.out.println(" Pls 2 for - Delete Epic.");
        System.out.println(" Pls 3 for - Delete SubTask.");
        System.out.println(" Pls 9 for - Main menu.");
    }
}
