package kk.practicum.code.yandex.daily.algoritm.basic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
//        int[] array = {1, 2, 65, 3, 5, 7, 54, 12, 46, 4, 2};
//        System.out.println("Индекс искомого элемента: " + find(array, 3));
//        System.out.println("Индекс искомого элемента: " + find(array, 46));
//        System.out.println("Индекс искомого элемента: " + find(array, 30));

//        randomNumberGame();


        int[] array1 = new int[]{1, 3, 4, 6, 8};
        int[] array2 = new int[]{1, 2, 3, 8, 9};
        int[] resultArray = merge(array1, array2);

        for (int e : resultArray) {
            System.out.print(e + ", ");
        }
    }

    private static int[] merge(int[] leftArray, int[] rightArray) {

        int leftSize = leftArray.length;
        int rightSize = rightArray.length;
        int[] resultArray = new int[leftSize + rightSize];

        int left = 0;
        int right = 0;
        int index = 0;

        while (index < resultArray.length) {

            // Если левый индекс больше максимального левого индекса — добавляем элемент из правого подмассива.
            if (left >= leftSize) {
                resultArray[index] = rightArray[right];
                right++;
            }

            // Если правый индекс больше максимального — добавляем элемент из левого подмассива.
            else if (right >= rightSize) {
                resultArray[index] = leftArray[left];
                left++;
            }

            // Если оба текущих индекса внутри своих границ, нужно сравнивать элементы по этим индексам
            // Если элемент в левом массиве меньше — добавляем его и увеличиваем левый индекс.
            else if (leftArray[left] <= rightArray[right]) {
                resultArray[index] = leftArray[left];
                left++;
            }
            // Иначе — делаем тоже самое с правым индексом.
            else {
                resultArray[index] = rightArray[right];
                right++;
                // !!! Добавьте ваш код
            }
            index++;
        }

        return resultArray;




    }

    private static void randomNumberGame() {
        Random random = new Random();
        int target = random.nextInt(1000) + 1;
        System.out.println("Я загадал число. Попробуйте угадать!");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int userTry = 0;


        for (; ; ) {
            try {
                ++userTry;
                String numberStr = reader.readLine();
                if (Integer.parseInt(numberStr) == target) {
                    System.out.println("The number is - " + target + " you use - " + userTry + " try.");
                    break;
                } else if (Integer.parseInt(numberStr) < target) {
                    System.out.println("Target is more. ");
                } else if (Integer.parseInt(numberStr) > target) {
                    System.out.println("Target is less. ");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


    public static int find(int[] array, int elem) {
        int index;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == elem) {
                return index = i;
            }
        }
        return -1000000;
    }


}
