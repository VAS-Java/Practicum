package kk.practicum.code.yandex.ft.tasktracker.structure;

import kk.practicum.code.yandex.ft.tasktracker.managers.HistoryManager;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Epic extends Task {


    private HashMap<Integer, Task> epicSubtasks = new HashMap<>();

    public Epic(String taskName, String taskDetails) {
        super(taskName, taskDetails);
    }

    @Override
    public void showDetails(HashMap<Integer, Task> allTasks, HashMap<Integer, Integer> subEpic, HistoryManager history) {
        super.showDetails(allTasks, subEpic, history);
        System.out.println("\nConsist of:");
        for (Task subtask : epicSubtasks.values()) {
            System.out.printf("\n%s %s %s ".formatted(subtask.getTaskName(), subtask.getTaskId(), subtask.getTaskDetails()));
        }
        System.out.println(" ");
    }

    @Override
    public void analysisStatus() {
        int counterNew = 0;
        int counterDone = 0;
        for (Task task : epicSubtasks.values()) {
            if (task.getCurrentStatus().equals(Status.NEW)) {
                counterNew += 1;
            } else if (task.getCurrentStatus().equals(Status.DONE)) {
                counterDone += 1;
            }
        }
        if (counterNew == epicSubtasks.size()) {
            this.setCurrentStatus(Status.NEW);
        } else if (counterDone == epicSubtasks.size()) {
            this.setCurrentStatus(Status.DONE);
        } else {
            this.setCurrentStatus(Status.IN_PROGRESS);
            ;
        }
    }

    public HashMap<Integer, Task> getEpicSubtasks() {
        return epicSubtasks;
    }

    public void setEpicSubtasks(Task subtask) {
        this.epicSubtasks.put(subtask.getTaskId(), subtask);
    }

    @Override
    public LocalTime getDurations() {
        LocalTime durationEpic = LocalTime.of(0, 0);
        if (!epicSubtasks.values().isEmpty()) {
            for (Task value : epicSubtasks.values()) {
                durationEpic = durationEpic.plus(value.getDurations().getHour(), ChronoUnit.HOURS)
                        .plus(value.getDurations().getMinute(), ChronoUnit.MINUTES);
            }
        }
        return durationEpic;
    }

    @Override
    public LocalDateTime getExpectedTime() {
        List<LocalDateTime> lstExpectedTime = new ArrayList<>();
        for (Task value : epicSubtasks.values()) {
          lstExpectedTime.add(value.getExpectedTime());
        }
        Optional<LocalDateTime> expectedTime = lstExpectedTime.stream().max(LocalDateTime::compareTo);

        return expectedTime.orElse(null);
    }


}
