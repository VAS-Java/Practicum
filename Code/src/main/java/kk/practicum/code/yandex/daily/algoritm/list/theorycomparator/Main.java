package kk.practicum.code.yandex.daily.algoritm.list.theorycomparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<Item> items = new ArrayList<>();
        items.add(new Item("Asus", 65000, 75));
        items.add(new Item("Acer", 55000, 75));
        items.add(new Item("MSI", 60000, 60));
        items.add(new Item("HP", 50000, 65));
        System.out.println(items);

        ItemPriceComporator itemPriceComporator = new ItemPriceComporator();
        items.sort(itemPriceComporator);
        System.out.println(items);
        ItemPopularityComparator itemPopularityComparator = new ItemPopularityComparator();
        items.sort(itemPopularityComparator);
        System.out.println(items);
        Comparator<Item> reversedItempopularityComparator = itemPopularityComparator.reversed();
        items.sort(reversedItempopularityComparator);
        System.out.println(items);
        ItemStringInSensitiveNameComparator itemStringInSensitiveNameComparator = new ItemStringInSensitiveNameComparator();
        items.sort(itemStringInSensitiveNameComparator);
        System.out.println(items);

    }
}
