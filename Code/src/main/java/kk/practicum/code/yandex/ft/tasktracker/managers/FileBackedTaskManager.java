package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Status;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;


public class FileBackedTaskManager extends InMemoryTaskManager implements TaskManager {


    public FileBackedTaskManager(HistoryManager history) {
        super(history);
    }

    @Override
    public void memoryGenerator() {
        String HOME = "src/main/java/kk/practicum/code/yandex/ft/tasktracker/data/";
        String line;

        try {
            BufferedReader br = new BufferedReader(new FileReader(HOME + "saveTracker.csv"));
            while ((line = br.readLine()) != null) {
                String[] lineUPD = line.split(",");
                if (lineUPD.length > 2) {
                    memoryGeneratorTask(lineUPD);
                } else if ((lineUPD.length == 2) && lineUPD[0].equals("history.log")) {
                    memoryGeneratorHistory(lineUPD);
                }
            }

        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private void memoryGeneratorHistory(String[] lineUPD) {
        if (allTasks.containsKey(Integer.parseInt(lineUPD[1].trim()))) {
            history.add(allTasks.get(Integer.parseInt(lineUPD[1].trim())));
        } else if (subEpic.containsKey(Integer.parseInt(lineUPD[1].trim()))) {
            Epic needEpic = (Epic) allTasks.get(subEpic.get(Integer.parseInt(lineUPD[1].trim())));
            history.add(needEpic.getEpicSubtasks().get(Integer.parseInt(lineUPD[1].trim())));
        }
    }

    private void memoryGeneratorTask(String[] lineUPD) {
        if (lineUPD[5].equals("null")) {
            if (lineUPD[1].equals("Epic")) {
                Task epicUPD = new Epic(lineUPD[2], lineUPD[3]);
//                            epicUPD.setCurrentStatus(Status.valueOf(lineUPD[4]));
                epicUPD.setStartTime(LocalDateTime.parse(lineUPD[6]));
                epicUPD.setDurations(LocalTime.parse(lineUPD[7]));
                allTasks.put(epicUPD.getTaskId(), epicUPD);
            } else {
                Task taskUPD = new SubTask(lineUPD[2], lineUPD[3]);
//                taskUPD.getStartTime(null);
//                taskUPD.getDurations(null);
                taskUPD.setCurrentStatus(Status.valueOf(lineUPD[4]));
                taskUPD.setStartTime(LocalDateTime.parse(lineUPD[6]));
                taskUPD.setDurations(LocalTime.parse(lineUPD[7]));
                allTasks.put(taskUPD.getTaskId(), taskUPD);
            }
        } else if (lineUPD[1].equals("SubTask")) {
            SubTask subUPD = new SubTask(lineUPD[2], lineUPD[3]);
            subUPD.setCurrentStatus(Status.valueOf(lineUPD[4]));
            subUPD.setConsistIn(Integer.parseInt(lineUPD[5]));
            subUPD.setStartTime(LocalDateTime.parse(lineUPD[6]));
            subUPD.setDurations(LocalTime.parse(lineUPD[7]));
            Epic epicUPD = (Epic) allTasks.get(Integer.parseInt(lineUPD[5]));
            epicUPD.getEpicSubtasks().put(subUPD.getTaskId(), subUPD);
            subEpic.put(subUPD.getTaskId(), subUPD.getConsistIn());

        }
    }


    public void save() {
        String HOME = "Practicum/Code/src/main/java/kk/practicum/code/yandex/ft/tasktracker/data/";

        File csvSaveTT = new File(HOME, "saveTracker.csv");
        File csvHistoryTT = new File(HOME, "saveHistory.csv");
        try {
            toStringTask(csvSaveTT);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void toStringTask(File csvSaveTT) throws IOException {
        BufferedWriter bw = Files.newBufferedWriter(csvSaveTT.toPath(), StandardCharsets.UTF_8);
        bw.write("id, type, name, details, status, consistIn, startTime, durations, expectedTime");
        for (Task task : allTasks.values()) {
            bw.append("\n%d,%s,%s,%s,%s,null,%s,%s,%s".formatted(
                    task.getTaskId(),
                    task.getClass().getSimpleName().replace("SubTask", "Task"),
                    task.getTaskName(),
                    task.getTaskDetails(),
                    task.getCurrentStatus(),
                    task.getStartTime(),
                    task.getDurations(),
                    task.getExpectedTime()

            ));
            if (task instanceof Epic) {
                for (Task subTusk : ((Epic) task).getEpicSubtasks().values()) {
                    bw.append("\n%d,%s,%s,%s,%s,%d,%s,%s,%s".formatted(
                            subTusk.getTaskId(),
                            subTusk.getClass().getSimpleName(),
                            subTusk.getTaskName(),
                            subTusk.getTaskDetails(),
                            subTusk.getCurrentStatus(),
                            ((SubTask) subTusk).getConsistIn(),
                            subTusk.getStartTime(),
                            subTusk.getDurations(),
                            subTusk.getExpectedTime()

                    ));
                }
            }
        }
        toStringHistory(bw);
        bw.close();
    }

    private void toStringHistory(BufferedWriter bw) throws IOException {
        for (Task task : history.getHistory()) {
            bw.append("\nhistory.log,%25d".formatted(task.getTaskId()));
        }
    }

    @Override
    public void showTasks() {
        super.showTasks();
        save();
    }


    @Override
    public void updateSubTask() {
        super.updateSubTask();
        save();
    }

    @Override
    public void updateEpic() {
        super.updateEpic();
        save();
    }

    @Override
    public void updateTask() {
        super.updateTask();
        save();
    }

    @Override
    public void createSubTask(SubTask subTask) {
        super.createSubTask(subTask);
        save();
    }

    @Override
    public void createEpic(Epic epic) {
        super.createEpic(epic);
        save();
    }

    @Override
    public void createTask(Task task) {
        super.createTask(task);
        save();
    }

    @Override
    public void createdUpdatedOptions(HashMap<Integer, Task> allTasks, HistoryManager history, BufferedReader reader) {
        super.createdUpdatedOptions(allTasks, history, reader);
        save();
    }

    @Override
    public void deleteTaskOrEpic() {
        super.deleteTaskOrEpic();
        save();
    }


    @Override
    public void deleteSubTask() {
        super.deleteSubTask();
        save();
    }

    @Override
    public void deleteAllTasks() {
        super.deleteAllTasks();
        save();
    }

    @Override
    public void deleteAllEpics() {
        super.deleteAllEpics();
        save();
    }

    @Override
    public void clearTracker() {
        super.clearTracker();
        save();
    }

    @Override
    public void exit() {
        save();
        super.exit();
    }




}
