package kk.practicum.code.yandex.ft.accounting;

import java.io.*;
import java.util.ArrayList;
import java.util.Objects;


public class Accounting implements AccountingInter {
    MonthReport report = new MonthReport();

    public Accounting() {}

    public void analise() {
        ArrayList<MonthData> newMonthData = null;
        String accountingHOME = "Practicum/Code/AccountingArrays/";
        BufferedReader accountingReader = null;
        String line;

        for (File file : Objects.requireNonNull(new File(accountingHOME).listFiles())) {
            String fileName = file.getName();
            if (fileName.startsWith("m")) {
                monthAnalysis(accountingHOME, fileName);

            } else if (fileName.startsWith("y")) {
                yearAnalysis(accountingHOME, fileName);
            }
        }
    }

    private void yearAnalysis(String accountingHOME, String fileName) {
        BufferedReader accountingReader;
        ArrayList<MonthData> newMonthData;
        String line;
        try {
            accountingReader = new BufferedReader(new FileReader(accountingHOME + fileName));
            newMonthData = new ArrayList<>();
            int counterData = 0;
            int counterMonth = 0;
            while ((line = accountingReader.readLine()) != null) {
                if (line.split(",")[0].startsWith("item")) {
                    counterData = 0;
                    counterMonth ++;
                    continue;
                }
                if (counterData == 3) {
                    report.addMonth("Y-month-0%d-%s".formatted(
                                    counterMonth,
                                    fileName.substring(1,5)),
                            newMonthData);
                    counterData = 0;
                    newMonthData = new ArrayList<>();
                }
                counterData++;
                if (parseContent(line) != null) {
                    newMonthData.add(parseContent(line));
                }
            }
            report.addMonth("Y-month-0%d-%s".formatted(
                            counterMonth,
                            fileName.substring(1,5)),
                    newMonthData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Невозможно прочитать файл. Error input data.");
        }
    }

    private void monthAnalysis(String accountingHOME, String fileName) {
        String line;
        ArrayList<MonthData> newMonthData;
        BufferedReader accountingReader;
        try {
            accountingReader = new BufferedReader(new FileReader(accountingHOME + fileName));
            newMonthData = new ArrayList<>();
            while ((line = accountingReader.readLine()) != null) {
                if (parseContent(line) != null) {
                    newMonthData.add(parseContent(line));
                }
            }
            report.addMonth("month-%s-%s".formatted(
                            fileName.substring(fileName.length() - 6, fileName.length() - 4),
                            fileName.substring(fileName.length() - 10, fileName.length() - 6)),
                    newMonthData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Невозможно прочитать файл. Error input data.");
        }
    }

    @Override
    public void comparison() {
        for (int i = 0; i < report.data.size() - 1 /2 ; i++) {
            ArrayList monthAccounting = report.data.get("month-0%d-2024".formatted(i));
            ArrayList yearAccounting = report.data.get("Y-month-0%d-2024".formatted(i));
            if (monthAccounting != null && yearAccounting != null) {
                System.out.println(monthAccounting);
                System.out.println(yearAccounting);
                System.out.println(monthAccounting.equals(yearAccounting));
            }
        }
    }

    @Override
    public MonthData parseContent(String content) {
        MonthData newMonth = null;
        String[] stringUPD = content.split(",");
        if (!stringUPD[0].equals("item_name") && stringUPD.length == 4) {
            newMonth = new MonthData(stringUPD[0],
                    Boolean.parseBoolean(stringUPD[1]),
                    Integer.parseInt(stringUPD[2]),
                    Integer.parseInt(stringUPD[3]));
        }
        return newMonth;
    }
}