package kk.practicum.code.yandex.ft.tasktracker.structure;

import kk.practicum.code.yandex.ft.tasktracker.analiz.Updating;
import kk.practicum.code.yandex.ft.tasktracker.managers.HistoryManager;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Objects;

public abstract class Task implements Updating, Details {

    private int taskId;
    private static int nextTaskId = 10_000;
    private String taskName;
    private String taskDetails;
    private Status currentStatus;

    private LocalDateTime startTime;
    private LocalTime durations ;
    private LocalDateTime expectedTime;

    public Task(String taskName, String taskDetails) {
        this.taskName = taskName;
        this.taskDetails = taskDetails;
        this.currentStatus = Status.NEW;
        this.taskId = nextTaskId++;
        this.startTime = LocalDateTime.now().minus(3, ChronoUnit.HOURS);
        this.durations = LocalTime.of(5,15,0);
    }

    public void analysisStatus() {}

    public void showDetails(HashMap<Integer, Task> allTasks, HashMap<Integer, Integer> subEpic, HistoryManager history) {
        if (allTasks.containsKey(this.getTaskId())) {
            history.add(allTasks.get(this.getTaskId()));
            System.out.printf("\nType - %s\nId is - %s\nName - %s\nStatus - %s\nDetails - %s\n"
                    .formatted(this.getClass().getSimpleName(), this.getTaskId(),this.getTaskName(),this.getCurrentStatus(),this.getTaskDetails()));
        } else if ((subEpic.containsKey(this.getTaskId()))) {
            Epic epicNeed = (Epic) allTasks.get(subEpic.get(this.getTaskId()));
            history.add(epicNeed.getEpicSubtasks().get(this.getTaskId()));
            System.out.printf("\nType - %s\nId is - %s\nName - %s\nStatus - %s\nDetails - %s\n"
                    .formatted(this.getClass().getSimpleName(), this.getTaskId(),this.getTaskName(),this.getCurrentStatus(),this.getTaskDetails()));
        }



    }

    public int getTaskId() {
        return taskId;
    }
    public Status getCurrentStatus() {
        return currentStatus;
    }
    public void setCurrentStatus(Status currentStatus) {this.currentStatus = currentStatus;}
    public String getTaskDetails() {
        return taskDetails;
    }
    public void setTaskDetails(String taskDetails) {
        this.taskDetails = taskDetails;
    }
    public String getTaskName() {
        return taskName;
    }
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public LocalDateTime getStartTime() {return startTime;}
    public void setStartTime(LocalDateTime startTime) {this.startTime = startTime;}
    public LocalTime getDurations() {return durations;}
    public void setDurations(LocalTime durations) {this.durations = durations;}
    public LocalDateTime getExpectedTime() {
        return expectedTime = startTime.plus(durations.getHour(),ChronoUnit.HOURS).plus(durations.getMinute(),ChronoUnit.MINUTES);
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return taskId == task.taskId && Objects.equals(taskName, task.taskName) && Objects.equals(taskDetails, task.taskDetails) && currentStatus == task.currentStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, taskName, taskDetails, currentStatus);
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskId=" + taskId +
                ", taskName='" + taskName + '\'' +
                ", taskDetails='" + taskDetails + '\'' +
                ", currentStatus=" + currentStatus +
                ", startTime=" + startTime +
                ", durations=" + durations +
                ", expectedTime=" + getExpectedTime() +
                '}';
    }
}
