package kk.practicum.code.yandex.daily.algoritm.hash;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ShoppingList {


    private static List<String> allPurchases = List.of(
            "яблоки",
            "молоко",
            "колбаса",
            "огурцы",
            "сок",
            "хлеб",
            "виноград",
            "молоко",
            "йогурт",
            "хлеб",
            "пельмени"
    );

    public static void main(String[] args) {

        System.out.println( "За месяц было куплено " + findUniquePurchases(allPurchases) + " уникальных товаров.");
    }

    // реализуйте этот метод
    public static Set<String> findUniquePurchases(List<String> allPurchases) {
        Set<String> uniquePurchases = new HashSet<>(allPurchases);
        return uniquePurchases;
    }



}
