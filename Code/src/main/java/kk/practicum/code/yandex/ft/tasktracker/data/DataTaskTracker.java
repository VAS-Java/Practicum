package kk.practicum.code.yandex.ft.tasktracker.data;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Status;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.util.HashMap;

public class DataTaskTracker {


    public HashMap<Integer, Task> arrTasks = new HashMap<>();

    public HashMap<Integer, Integer> arrSubEpic = new HashMap<>();

    public DataTaskTracker() {

        Epic taskATC = new Epic("RDC", "ATC");
        SubTask taskOvdHandle = new SubTask("Job", "ATC-Handle");
        taskOvdHandle.setCurrentStatus(Status.DONE);
        taskOvdHandle.setConsistIn(taskATC.getTaskId());
        arrSubEpic.put(taskOvdHandle.getTaskId(), taskATC.getTaskId());
        SubTask taskOvdPlan = new SubTask("Job", "ATC-plan");
        taskOvdPlan.setCurrentStatus(Status.DONE);
        taskOvdPlan.setConsistIn(taskATC.getTaskId());
        arrSubEpic.put(taskOvdPlan.getTaskId(), taskATC.getTaskId());
        taskATC.setEpicSubtasks(taskOvdHandle);
        taskATC.setEpicSubtasks(taskOvdPlan);


        arrTasks.put(taskATC.getTaskId(), taskATC);




        Epic taskDCP = new Epic("RDC", "DCP");
        SubTask subTaskDCPTF = new SubTask("Job", "DCP-Transport");
        subTaskDCPTF.setCurrentStatus(Status.DONE);
        subTaskDCPTF.setConsistIn(taskDCP.getTaskId());
        SubTask subTaskDCPMF = new SubTask("Job", "DCP-Military-Transition-Flights");
        subTaskDCPMF.setCurrentStatus(Status.IN_PROGRESS);
        subTaskDCPMF.setConsistIn(taskDCP.getTaskId());
        SubTask subTaskDCPAF = new SubTask("Job", "DCP-Aviation_Chemistry-Job");
        subTaskDCPAF.setCurrentStatus(Status.IN_PROGRESS);
        subTaskDCPAF.setConsistIn(taskDCP.getTaskId());
        SubTask subTaskDCPSpecialsF = new SubTask("Job", "DCP-Senior-Navigator");
        subTaskDCPSpecialsF.setConsistIn(taskDCP.getTaskId());
        taskDCP.setEpicSubtasks(subTaskDCPTF);
        taskDCP.setEpicSubtasks(subTaskDCPMF);
        taskDCP.setEpicSubtasks(subTaskDCPAF);
        taskDCP.setEpicSubtasks(subTaskDCPSpecialsF);
        arrSubEpic.put(subTaskDCPTF.getTaskId(), taskDCP.getTaskId());
        arrSubEpic.put(subTaskDCPMF.getTaskId(), taskDCP.getTaskId());
        arrSubEpic.put(subTaskDCPAF.getTaskId(), taskDCP.getTaskId());
        arrSubEpic.put(subTaskDCPSpecialsF.getTaskId(), taskDCP.getTaskId());

        arrTasks.put(taskDCP.getTaskId(), taskDCP);

        Task veterinaryDelivery = new SubTask("Veterinary", "Delivery");
        arrTasks.put(veterinaryDelivery.getTaskId(), veterinaryDelivery);
        Task veterinaryCleaning = new SubTask("Veterinary", "Cleaning");
        arrTasks.put(veterinaryCleaning.getTaskId(), veterinaryCleaning);


        for (Task task : arrTasks.values()) {
            task.analysisStatus();
        }


    }


}
