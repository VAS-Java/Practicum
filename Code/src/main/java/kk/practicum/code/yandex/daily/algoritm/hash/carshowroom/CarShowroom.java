package kk.practicum.code.yandex.daily.algoritm.hash.carshowroom;


import java.util.*;

public class CarShowroom {


    public static void main(String[] args) {
        Map<Car, Integer> cars = new TreeMap<>(new  CarPriceComparator().reversed());


        cars.put(new Car("Audi A6", 10_000_000), 2);
        cars.put(new Car("Honda CR-V", 5_000_000), 3);
        cars.put(new Car("KIA Cerato", 3_000_000), 8);
        cars.put(new Car("Skoda Rapid", 2_000_000), 5);
        cars.put(new Car("BMV X5", 15_000_000), 10);
        cars.put(new Car("Audi A4", 7_000_000), 7);


        for (Car item : cars.keySet()) {
            System.out.println(item);
        }

    }



}
