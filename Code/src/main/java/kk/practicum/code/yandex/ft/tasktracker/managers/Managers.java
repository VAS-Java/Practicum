package kk.practicum.code.yandex.ft.tasktracker.managers;

public class Managers {

    public static void main(String[] args) {
//        getDefault();
        getBackedDefault();
    }

    public static TaskManager getDefault() {
        return new InMemoryTaskManager(getDefaultHistory()).runner();
    }

    public static TaskManager getBackedDefault() {
        return new FileBackedTaskManager(getDefaultHistory()).runner();
    }

    public static TaskManager getHttp() {return new BackedHttpManager(getDefaultHistory());}

    public static HistoryManager getDefaultHistory() {
        return new InMemoryHistoryManager();
    }


}
