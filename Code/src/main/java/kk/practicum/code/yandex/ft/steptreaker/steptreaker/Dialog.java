package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Dialog {
    BufferedReader reader;
    StepTracker stepTracker;


    public Dialog(StepTracker stepTracker) {
        this.stepTracker = stepTracker;
        this.reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public void starter() {
        System.out.println("\nThis is main menu StepTracker. Version 2.0 betta.\n");

    }

    static void trackerMenu() {
        System.out.println("\n1. В вести колличество шагов за день.");
        System.out.println("2. Напечатать статистику за месяц.");
        System.out.println("3. Изменить цель по колличеству шагов в день.");
        System.out.println("4. Выйти из приложения.\n");
    }

    void inputError() {
        System.out.println("\nВведите корректную команду из предложенных.\n");
    }

}






