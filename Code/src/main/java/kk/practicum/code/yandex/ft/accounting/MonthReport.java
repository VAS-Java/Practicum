package kk.practicum.code.yandex.ft.accounting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class MonthReport {

    HashMap<String, ArrayList<MonthData>> data = new HashMap<>();

    public MonthReport() {}


    public void addMonth(String month, ArrayList<MonthData> monthData) {
        data.put(month, monthData);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonthReport report = (MonthReport) o;
        return Objects.equals(data, report.data);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(data);
    }

    @Override
    public String toString() {
        return "MonthReport{" +
                "data=" + data +
                '}';
    }
}
