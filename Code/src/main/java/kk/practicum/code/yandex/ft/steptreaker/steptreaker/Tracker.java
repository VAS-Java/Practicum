package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;

public class Tracker {

    public static void main(String[] args) {


        StepTracker trackerRun = new StepTracker();

        trackerRun.createYear(LocalDate.now().getYear());

        AdditionalInfo info = new AdditionalInfo(trackerRun);
        info.arrInfo(trackerRun);
        Dialog dialog = new Dialog(trackerRun);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        dialog.starter();
        for (; ; ) {
            try {
                dialog.trackerMenu();

                int choiceInit = Integer.parseInt(reader.readLine());
                switch (choiceInit) {
                    case (1):
                        trackerRun.addSteps();
                        break;
                    case (2):
                        new Statistick(trackerRun, trackerRun.showMonth()).printStatistic();
                        break;
                    case (3):
                        trackerRun.changeDailySteps();
                        break;
                    case (4):
                        System.out.println("Exit");
                        throw new RuntimeException();
                    default:
                        dialog.inputError();
                        break;
                }
            } catch (IOException e) {
                System.out.println();

            }
        }
    }
}


