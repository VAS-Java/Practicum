package kk.practicum.code.yandex.ft.tasktracker.options;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;
import kk.practicum.code.yandex.ft.tasktracker.managers.HistoryManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class InformationOptions {

    HashMap<Integer, Task> allTasks;
    HashMap<Integer, Integer> subEpic;
    HistoryManager history;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


    public InformationOptions(HashMap<Integer, Task> allTasks, HashMap<Integer, Integer> subEpic, HistoryManager history) {
        this.allTasks = allTasks;
        this.subEpic = subEpic;
        this.history = history;
    }

    public void showTasks() {
        if (allTasks.isEmpty()) {
            System.out.println("Task's array empty!");
        }
        for (Task task : allTasks.values()) {
            task.analysisStatus();
//            System.out.println(task.getClass().getSimpleName() + " Id is -  " + task.getTaskId() + " " + task.getTaskName() + " " + task.getCurrentStatus());
            System.out.println(task.toString());
        }
    }

    public void detailInformation() {
        System.out.println("Input Task - ID.");
        try {
            int taskId = Integer.parseInt(reader.readLine());
            if (allTasks.containsKey(taskId)) {
                allTasks.get(taskId).analysisStatus();
                allTasks.get(taskId).showDetails(allTasks, subEpic, history);
            }else if (subEpic.containsKey(taskId)) {
                Epic epicUpd = (Epic) allTasks.get(subEpic.get(taskId));
                epicUpd.analysisStatus();
                epicUpd.getEpicSubtasks().get(taskId).showDetails(allTasks,subEpic, history);
            }

        } catch (IOException e) {
            System.out.println("Unpredictable ID in input.");
        }

    }

    public void initMenu() {

        System.out.println(" Pls 1 for - Show all Task's.");
        System.out.println(" Pls 2 for - Detail information.");
        System.out.println(" Pls 3 for - Delete menu.");
        System.out.println(" Pls 4 for - Create & Update menu.");
        System.out.println(" Pls 8 for - History information's.");
        System.out.println(" Pls 9 for - Exit.");
//        System.out.println(" Pls   for - ");
    }


//    public Task getTask(int taskID){
//        return  allTasks.get(taskID);
//    };
//    public Task getEpic(int epicID){
//        return allTasks.get(epicID);
//    };
//    public SubTask getSubTask(int subTaskId){
//        allTasks.get(subTaskId);
//        return null;};


}
