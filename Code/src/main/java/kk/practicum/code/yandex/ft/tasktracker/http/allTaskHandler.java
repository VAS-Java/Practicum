package kk.practicum.code.yandex.ft.tasktracker.http;

import com.google.gson.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import kk.practicum.code.yandex.ft.tasktracker.managers.BackedHttpManager;
import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class allTaskHandler implements HttpHandler {

    BackedHttpManager manager;
    private Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

    public allTaskHandler(BackedHttpManager manager) {
        this.manager = manager;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if (exchange.getRequestMethod().equals("GET")) {
            exchange.sendResponseHeaders(200, 0);

            exchange.getRequestMethod();
            System.out.println(exchange.getRequestMethod());

            try (OutputStream os = exchange.getResponseBody()) {
                for (Task task : manager.getAllTasks().values()) {


                    System.out.println(gson.toJson(String.valueOf(task)));


                    os.write("%s\n".formatted(gson.toJson(String.valueOf(task))).getBytes(StandardCharsets.UTF_8));
                    if (task instanceof Epic) {
                        HashMap<Integer, Task> subTsk = ((Epic) task).getEpicSubtasks();
                        for (Task value : subTsk.values()) {
                            os.write(gson.toJson(String.valueOf(value)).getBytes(StandardCharsets.UTF_8));
                        }


                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!exchange.getRequestMethod().equals("GET")) {
            exchange.sendResponseHeaders(500, 0);
        }


    }


}
