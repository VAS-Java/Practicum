package kk.practicum.code.yandex.daily.algoritm.hash;


import java.time.LocalDate;
import java.util.*;


public class Correspondence {

    private static Set<Letter> letters = new LinkedHashSet();

    public static void main(String[] args) {
        letters.add(new Letter("Джон Смит", LocalDate.of(2021, 7, 7), "текст письма №1 ..."));
        letters.add(new Letter("Аманда Линс", LocalDate.of(2021, 6, 17), "текст письма №2 ..."));
        letters.add(new Letter("Джо Кью", LocalDate.of(2021, 7, 5), "текст письма №3 ..."));
        letters.add(new Letter("Мишель Фернандес", LocalDate.of(2021, 8, 23), "текст письма №4 ..."));

        printOrderedById(letters);
        printOrderedByDateReceived(letters);
    }

    private static void printOrderedById(Set<Letter> letters) {
        System.out.println("Все письма  - ");
        for (Letter letter : letters) {
            System.out.println("Письмо от - " + letter.authorName + ". Поступило - " + letter.dateReceived);
        }
        System.out.println("Все письма с сортировкой по ID - ");
        Set<Letter> lettersID = new TreeSet<>(Comparator.comparing(letter -> letter.dateReceived));
        lettersID.addAll(letters);
        for (Letter letter : lettersID) {
            System.out.println("Письмо от - " + letter.authorName + ". Поступило - " + letter.dateReceived);
        }
    }

    private static void printOrderedByDateReceived(Set<Letter> letters) {
        System.out.println("");

    }

    static class Letter {
        public String authorName;
        public LocalDate dateReceived;
        public String text;

        public Letter(String senderName, LocalDate dateReceived, String text) {
            this.authorName = senderName;
            this.dateReceived = dateReceived;
            this.text = text;
        }
    }

    public class dateComparator implements Comparator<Letter> {

        @Override
        public int compare(Letter o1, Letter o2) {
            if (o1.dateReceived.isAfter(o2.dateReceived)) {
                return 1;
            } else if (o1.dateReceived.isBefore(o2.dateReceived)) {
                return -1;
            } else {
                return 0;
            }
        }
    }


}
