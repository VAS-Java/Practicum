package kk.practicum.code.yandex.daily.algoritm.hash;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

public class SearchID {

    private static Map<Long, User> users = new HashMap();


    public static void main(String[] args) {


        // создадим 10 миллион пользователей
        for (long i = 1; i <= 1_000_000L; i++) {
            User newUser = new User("User --- " + i);
            users.put(newUser.getCurID(), newUser);
        }

        final long startTime = System.nanoTime();
        User user = findUser(378_366L);
        final long endTime = System.nanoTime();

        System.out.println("Найден пользователь: " + user);
        System.out.println("Поиск занял " + (endTime - startTime) + " наносекунд.");


    }


    private static User findUser(Long userId) {
        return users.get(userId);
    }


    static class User {
        private static Long id = 0L;
        private Long curID;
        private String name;

        public User(String name) {
            ++id;
            this.curID = id;
            this.name = name;
        }

        public Long getCurID() {
            return curID;
        }

        public String toString() {
            return "User{id=" + curID + ", name='" + name + "'}";
        }
    }

}
