package kk.practicum.code.yandex.daily.algoritm.hash.carshowroom;

import java.util.Comparator;

public class CarPriceComparator implements Comparator<Car> {

    @Override
    public int compare(Car car, Car carAdd) {
        return car.getPriceInRubles() - carAdd.getPriceInRubles();
    }

}
