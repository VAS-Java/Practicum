package kk.practicum.code.yandex.ft.tasktracker.structure;

public enum Status {
    NEW,
    IN_PROGRESS,
    DONE
}
