package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;


public class StepTracker implements TrackerInfo {

    private int stepsDaily = 10_000;

    private HashMap<Integer, Year> years = new HashMap<>();

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public int showMonth() {
        int choisingMonthNumber = 0;
        Integer currentYear = LocalDate.now().getYear();
        System.out.printf("---%d---\n".formatted(currentYear));
        for (int i = 0; i < years.get(currentYear).dataYear.keySet().size(); i++) {
            System.out.printf("\n%d for - %s. ".formatted(i + 1, Months.values()[i]));
        }
        try {
            System.out.println("\nInput month for statistic!");
            choisingMonthNumber = Integer.parseInt(reader.readLine());
            Month choisingMonth = years.get(currentYear).dataYear.get(choisingMonthNumber);
            System.out.println(choisingMonth);
        } catch (IOException e) {
            System.out.println(e);
        }
        return  choisingMonthNumber;
    }

    @Override
    public void createYear(Integer year) {
        Year newYear = new Year(year);
        years.put(year, newYear);
    }

    @Override
    public void addSteps() {
        Integer currentYear = LocalDate.now().getYear();
        System.out.printf("---%d---\n".formatted(currentYear));
        for (int i = 0; i < years.get(currentYear).dataYear.keySet().size(); i++) {
            System.out.printf("\n%d for - %s. ".formatted(i + 1, Months.values()[i]));
        }
        try {
            System.out.println("\nInput month to add steps");
            Month choisingMonth = years.get(currentYear).dataYear.get(Integer.parseInt(reader.readLine()));
            System.out.println(choisingMonth);
            System.out.println("\nInput day to add steps");
            ArrayList<Integer> choisingDay = choisingMonth.stepsPerDays.get(Integer.parseInt(reader.readLine()));
            System.out.println("Input steps");
            int inputSteps = Integer.parseInt(reader.readLine());
            if (inputSteps > 0) {
                choisingDay.add(inputSteps);
            } else {
                System.out.println("Unpredictable value!");
            }

        } catch (IOException e) {
            System.out.println(e.toString());
        }

    }

    @Override
    public void changeDailySteps() {
        System.out.printf("Input new daily number of daily steps. previous - %d\n".formatted(getStepsDaily()));
        try {
            setStepsDaily(Integer.parseInt(reader.readLine()));
        } catch (IOException e) {
            System.out.println("Input correct number!");
        }

    }

    @Override
    public void addStepsTest(int month,int day,int steps) {
        years.get(LocalDate.now().getYear()).getDataYear().get(month).stepsPerDays.get(day).add(steps);

    }

    public int getStepsDaily() {
        return stepsDaily;
    }

    public void setStepsDaily(int stepsDaily) {
        this.stepsDaily = stepsDaily;
    }

    public HashMap<Integer, Year> getYears() {
        return years;
    }

}