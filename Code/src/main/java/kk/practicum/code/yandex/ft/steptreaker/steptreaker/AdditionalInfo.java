package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

public class AdditionalInfo {

    StepTracker trackerRun;

    public AdditionalInfo(StepTracker trackerRun) {
        this.trackerRun = trackerRun;
    }

    void arrInfo(StepTracker trackerRun){

        trackerRun.addStepsTest(1,1,5000);
        trackerRun.addStepsTest(1,2,5000);
        trackerRun.addStepsTest(1,3,5000);
        trackerRun.addStepsTest(1,4,5000);
        trackerRun.addStepsTest(1,5,5000);

        trackerRun.addStepsTest(2,1,5000);
        trackerRun.addStepsTest(2,2,12000);
        trackerRun.addStepsTest(2,3,15000);
        trackerRun.addStepsTest(2,4,11000);
        trackerRun.addStepsTest(2,5,5000);

        trackerRun.addStepsTest(3,1,5000);
        trackerRun.addStepsTest(3,2,5000);
        trackerRun.addStepsTest(3,3,5000);
        trackerRun.addStepsTest(3,4,5000);
        trackerRun.addStepsTest(3,5,5000);

        trackerRun.addStepsTest(4,1,5000);
        trackerRun.addStepsTest(4,2,5000);
        trackerRun.addStepsTest(4,3,5000);
        trackerRun.addStepsTest(4,4,5000);
        trackerRun.addStepsTest(4,5,5000);

        trackerRun.addStepsTest(5,1,5000);
        trackerRun.addStepsTest(5,2,5000);
        trackerRun.addStepsTest(5,3,5000);
        trackerRun.addStepsTest(5,4,5000);
        trackerRun.addStepsTest(5,5,5000);

        trackerRun.addStepsTest(6,1,5000);
        trackerRun.addStepsTest(6,2,5000);
        trackerRun.addStepsTest(6,3,5000);
        trackerRun.addStepsTest(6,4,5000);
        trackerRun.addStepsTest(6,5,5000);

        trackerRun.addStepsTest(7,1,5000);
        trackerRun.addStepsTest(7,2,5000);
        trackerRun.addStepsTest(7,3,5000);
        trackerRun.addStepsTest(7,4,5000);
        trackerRun.addStepsTest(7,5,5000);

        trackerRun.addStepsTest(8,1,5000);
        trackerRun.addStepsTest(8,2,5000);
        trackerRun.addStepsTest(8,3,5000);
        trackerRun.addStepsTest(8,4,5000);
        trackerRun.addStepsTest(8,5,5000);

        trackerRun.addStepsTest(9,1,5000);
        trackerRun.addStepsTest(9,2,5000);
        trackerRun.addStepsTest(9,3,5000);
        trackerRun.addStepsTest(9,4,5000);
        trackerRun.addStepsTest(9,5,5000);

        trackerRun.addStepsTest(10,1,5000);
        trackerRun.addStepsTest(10,2,5000);
        trackerRun.addStepsTest(10,3,5000);
        trackerRun.addStepsTest(10,4,5000);
        trackerRun.addStepsTest(10,5,5000);

        trackerRun.addStepsTest(11,1,5000);
        trackerRun.addStepsTest(11,2,5000);
        trackerRun.addStepsTest(11,3,5000);
        trackerRun.addStepsTest(11,4,5000);
        trackerRun.addStepsTest(11,5,5000);

        trackerRun.addStepsTest(12,1,5000);
        trackerRun.addStepsTest(12,2,5000);
        trackerRun.addStepsTest(12,3,5000);
        trackerRun.addStepsTest(12,4,5000);
        trackerRun.addStepsTest(12,5,5000);

    }

}
