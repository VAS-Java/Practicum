package kk.practicum.code.yandex.daily.algoritm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Polidrom {


    public static void main(String[] args) {
        System.out.println(polidrom("VAS"));
        System.out.println(polidrom("VAV"));
        System.out.println(polidrom("MAAM"));
        System.out.println("Generator");
        System.out.println(polidromGenerator("MAMAMA"));
        System.out.println(polidromGenerator("MAMA"));
        System.out.println(polidromGenerator("MAMAMAL"));
        System.out.println(polidromGenerator("MAMAMALO"));
        System.out.println(polidromGenerator("MAMAMAAALO"));
        System.out.println(polidromGenerator("MAMAMAAALOYYYYYYY"));
        System.out.println(polidromGenerator("MAMAMMMMMMMMMAAALLLLLLLOOOYYYYYYYYYYYYYPPPPPPPPPPPPPPPPPPPPP"));
        System.out.println(polidromGenerator("MAMAMMMMMMMMMAAALLLLLLLOOOYYYYYiuYYYYYYYYPPPPPPPPPPPPPPPPPPPPP"));
        System.out.println(polidromGenerator("MAMALLL"));
        ;
    }

    public static boolean polidrom(String string) {
        String[] lst = string.split("");
        for (int i = 0; i < string.length() / 2 + 1; i++) {
            if (!(lst[i].equals(lst[string.length() - 1 - i]))) {
                return false;
            }
        }
        return true;
    }

    public static boolean polidromGenerator(String string) {

        Map<String, Integer> mapInfo = new HashMap<>();
        String[] lstInfo = string.split("");
        Integer counterInfo = 0;

        for (int i = 0; i < string.length(); i++) {
            mapInfo.put(lstInfo[i], mapInfo.getOrDefault(lstInfo[i], 0) + 1);
        }

        ArrayList<Integer> arrInfo = new ArrayList<>(List.copyOf(mapInfo.values()));

        for (Integer item : arrInfo) {
            if ((item == 1) || (item % 2 == 1)) {
                counterInfo++;
            }
        }

        return counterInfo > 1 ? false: true ;
    }

}
