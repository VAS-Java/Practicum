package kk.practicum.code.yandex.daily.algoritm;

import java.util.HashMap;

public class TestLruCache {

    public static void main(String[] args) {

        LRUCache cache = new LRUCache();

        cache.getValue("VAS");
        cache.getValue("VAKL");
        cache.getValue("VKA");
        cache.getValue("VTA");
        cache.getValue("VAS");
        cache.getValue("VKA");
        cache.getValue("VLA");
        cache.getValue("VLA");


    }

}


class LRUCache {

    HashMap<String, CacheNode> storage = new HashMap<>();
    CacheNode mru = null;
    CacheNode lru = null;


    public Long getValue(String key) {
        if (!storage.containsKey(key)) {

            if (storage.size() > 5) {
                storage.remove(this.lru.key);
                this.lru = this.lru.next;
                this.lru.previous = null;
            }

            CacheNode newNode = new CacheNode(key, System.currentTimeMillis(), mru, null);
            if (this.mru != null) {
                this.mru.next = newNode;
            }

            if (this.lru == null) {
                this.lru = newNode;
            }
            this.mru = newNode;
            this.storage.put(key, this.mru);
            return this.mru.value;

        }

        CacheNode cached = storage.get(key);
        if (cached == null) {
            return cached.value;
        }

        CacheNode nextToCached = cached.next;
        nextToCached.previous = cached.previous;
        if (cached.previous != null) {
            cached.previous.next = cached.next;
        }
        cached.next = null;
        cached.previous = mru;
        this.mru = cached;
        return cached.getValue();

    }

    public void addValue() {
        System.out.println();
    }


}


class CacheNode {

    String key;
    long value;
    CacheNode next;
    CacheNode previous;


    public CacheNode(String key, long value, CacheNode next, CacheNode previous) {
        this.key = key;
        this.value = value;
        this.next = next;
        this.previous = previous;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public CacheNode getNext() {
        return next;
    }

    public void setNext(CacheNode next) {
        this.next = next;
    }

    public CacheNode getPrevious() {
        return previous;
    }

    public void setPrevious(CacheNode previous) {
        this.previous = previous;
    }


}



