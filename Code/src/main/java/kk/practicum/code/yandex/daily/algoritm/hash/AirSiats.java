package kk.practicum.code.yandex.daily.algoritm.hash;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public interface AirSiats {

    List<String> passengerNames = List.of(
            "Василий Петров",
            "Анна Ягирская",
            "Виктория Сотова",
            "Игорь Серов",
            "Людмила Ульянова"
    );

    public static void main(String[] args) {
        Map<String, Integer> seats = assignSeats(passengerNames);
        System.out.println("Места пассажиров: " + seats);

    }

    public static Map<String, Integer> assignSeats(List<String> passengerNames) {
        Map <String, Integer> airSeats = new HashMap<>();
        for (int i = 0; i < passengerNames.size() ; i++) {
            airSeats.put(passengerNames.get(i), i +1);
        }

        return airSeats;
    }

}