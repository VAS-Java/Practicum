package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.util.*;

public class InMemoryHistoryManager implements HistoryManager {

    List<Task> historyData = new LinkedList<>();
    Map<Integer, Integer> historyInfo = new HashMap<>();



    public void add(Task task) {
        if (historyData.contains(task)) {
            remote(task.getTaskId());
        }
        historyData.add(task);
        historyInfo.put(task.getTaskId(), historyData.size());

    }

    public void remote(int id) {
        if (!historyInfo.isEmpty()) {
            if (id == historyData.getFirst().getTaskId()) {
                historyData.removeFirst();
                analysis(historyInfo.get(id));
                historyInfo.remove(id);


            } else if (id == historyData.getLast().getTaskId()) {
                historyData.removeLast();
                historyInfo.remove(id);

            }else {
                historyData.remove(historyInfo.get(id) - 1);
                analysis(historyInfo.get(id));
                historyInfo.remove(id);
            }
        }
    }

    public List<Task> getHistory() {
        return new ArrayList<>(historyData);
    }

    public void clear() {
        historyData.clear();
        historyInfo.clear();
    }


    public void analysis(int number) {
        for (Integer value : historyInfo.values()) {
            if (value >= number) {
                value--;
            }
        }
    }

}
