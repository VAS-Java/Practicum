package kk.practicum.code.yandex.ft.accounting;

import java.util.Objects;

public class MonthData {
    String itemName;
    boolean isExpense;
    float price;
    int count;

    public MonthData(String itemName, boolean isExpense, int count, float price) {
        this.itemName = itemName;
        this.isExpense = isExpense;
        this.count = count;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonthData monthData = (MonthData) o;
        return isExpense == monthData.isExpense && Float.compare(price, monthData.price) == 0 && count == monthData.count && Objects.equals(itemName, monthData.itemName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemName, isExpense, price, count);
    }

    @Override
    public String toString() {
        return "MonthData{" +
                "count=" + count +
                ", itemName='" + itemName + '\'' +
                ", isExpense=" + isExpense +
                ", price=" + price +
                '}';
    }
}
