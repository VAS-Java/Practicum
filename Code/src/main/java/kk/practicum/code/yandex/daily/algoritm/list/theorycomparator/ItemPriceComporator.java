package kk.practicum.code.yandex.daily.algoritm.list.theorycomparator;

import java.util.Comparator;

public class ItemPriceComporator implements Comparator<Item> {


    @Override
    public int compare(Item i1, Item i2) {
        return i1.price - i2.price;
    }
}
