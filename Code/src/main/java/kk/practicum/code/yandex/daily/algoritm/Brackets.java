package kk.practicum.code.yandex.daily.algoritm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Brackets {

    public static void main(String[] args) {

        System.out.println(brackets("(([][]){})"));
        System.out.println(brackets("(([][]){}]"));
        System.out.println(brackets("(([][]){[}{]})"));


    }

    static boolean brackets(String string) {
        String [] lstString = string.split("");
        ArrayList<String> stek = new ArrayList<>();
        List<String> open = new ArrayList(Arrays.asList("(", "[", "{"));
        for (String litem : lstString) {
            if (open.contains(litem)) {
                stek.add(litem);
                continue;
            }
            String lastBrackets = stek.removeLast();
            if ((lastBrackets.equals("(") && litem.equals(")"))
            || (lastBrackets.equals("[") && litem.equals("]"))
            || (lastBrackets.equals("{") && litem.equals("}"))) {
                continue;
            }else {
                return false;
            }

        }

        return stek.isEmpty() ? true : false;

    }





}
