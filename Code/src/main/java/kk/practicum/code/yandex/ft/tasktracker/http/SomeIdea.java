package kk.practicum.code.yandex.ft.tasktracker.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpServer;
import kk.practicum.code.yandex.ft.tasktracker.managers.BackedHttpManager;
import kk.practicum.code.yandex.ft.tasktracker.managers.Managers;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class SomeIdea {


    private static final int PORT = 8080;
    private final BackedHttpManager manager;
    private HttpServer httpServerManager;
    private Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

    public SomeIdea(BackedHttpManager manager) {
        this.manager = manager;
        try  {
            this.httpServerManager = HttpServer.create();
            this.httpServerManager.bind(new InetSocketAddress(PORT), 0);
            this.httpServerManager.createContext("/tasks", exchange -> {

                HashMap<Integer, Task> allTask = this.manager.getAllTasks();
                System.out.println(allTask);
                String[] splitURI = exchange.getRequestURI().getPath().split("/");
                String query = exchange.getRequestURI().getQuery();
                String response = "";
                System.out.println(splitURI);
                System.out.println(query);
                System.out.println(response);
                switch (exchange.getRequestMethod()) {
                    case "GET":
                        exchange.sendResponseHeaders(200, 0);
                        System.out.println("GET");
                        response = "Hello -- VAS";
                        break;

                    case "POST":
                        exchange.sendResponseHeaders(201, 0);
                        System.out.println("POST");
                        break;
                    case "DELETE":
                        exchange.sendResponseHeaders(202, 0);
                        System.out.println("DELETE");
                        break;
                }
                try (OutputStream os = exchange.getResponseBody()) {
                    os.write(response.getBytes(StandardCharsets.UTF_8));
                }

            });
            this.httpServerManager.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new SomeIdea((BackedHttpManager) Managers.getHttp());
    }

}
