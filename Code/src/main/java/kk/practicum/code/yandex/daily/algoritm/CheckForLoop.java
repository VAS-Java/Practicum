package kk.practicum.code.yandex.daily.algoritm;

import java.util.HashSet;

public class CheckForLoop {


    public static void main(String[] args) {


        Node node1 = new Node(null, 1);
        Node node2 = new Node(null, 2);
        Node node3 = new Node(null, 3);
        Node node4 = new Node(null, 4);
        Node node5 = new Node(null, 5);
        Node node6 = new Node(null, 6);
        Node node7 = new Node(null, 7);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);
        node4.setNext(node5);
//        node5.setNext(node6);
        node6.setNext(node7);
        node7.setNext(node1);

        System.out.println(checkForLoop(node5));

    }

    static boolean checkForLoop(Node startNode) {
        HashSet<Node> visited = new HashSet<>();
        while (startNode.next != null) {
            if (visited.contains(startNode)) {
                return false;
            }
            visited.add(startNode);
            startNode = startNode.next;
        }
        return true;

    }
}



class Node {
    int value;

    Node next;
    boolean visited = false;

    public Node(Node next, int value) {
        this.next = next;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}


