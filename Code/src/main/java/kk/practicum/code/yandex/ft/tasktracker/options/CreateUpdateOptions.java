package kk.practicum.code.yandex.ft.tasktracker.options;

import kk.practicum.code.yandex.ft.tasktracker.managers.HistoryManager;
import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Status;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class CreateUpdateOptions {

    HashMap<Integer, Task> allTasks;
    HashMap<Integer, Integer> subEpic;
    HistoryManager history;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public CreateUpdateOptions(HashMap<Integer, Task> allTasks,HashMap<Integer, Integer> subEpic , HistoryManager history) {
        this.allTasks = allTasks;
        this.history = history;
        this.subEpic = subEpic;
    }


    public Task createTaskPreparation() {
        try {
            System.out.println("Create the Task.");
            System.out.println("Input task name.");
            String taskName = reader.readLine();
            System.out.println("Input task details.");
            String taskDetails = reader.readLine();
            Task newTask = new SubTask(taskName, taskDetails);
            return newTask;
        } catch (Exception e) {
            System.out.println("Error input.");
        }
        return null;
    }

    public void createTask(Task task) {
        allTasks.put(task.getTaskId(), task);
    }

    public Epic createEpikPreparation() {
        try {
            System.out.println("Create the epic.");
            System.out.println("Input epic name.");
            String epicName = reader.readLine();
            System.out.println("Input epic details.");
            String epicDetails = reader.readLine();
            Epic newEpic = new Epic(epicName, epicDetails);
            return newEpic;
        } catch (Exception e) {
            System.out.println("Error input.");
        }
        return null;
    }

    public void createEpic(Epic epic) {
        allTasks.put(epic.getTaskId(), epic);

    }

    public void createSubTask(SubTask subTask) {
        Epic updEpic = (Epic) allTasks.get(subTask.getConsistIn());
        updEpic.getEpicSubtasks().put(subTask.getTaskId(),subTask);
        subEpic.put(subTask.getTaskId(), updEpic.getTaskId());

    }

    public SubTask createSubtaskPreparation() {
        try {
            System.out.println("Create the subtask.");
            System.out.println("Input the epic ID for subtask.");
            String epicID = reader.readLine();
            System.out.println("Input subtask name.");
            String subTaskName = reader.readLine();
            System.out.println("Input subtask details.");
            String subTaskDetails = reader.readLine();
            SubTask newSubTask = new SubTask(subTaskName, subTaskDetails);
            newSubTask.setConsistIn(Integer.parseInt(epicID));
            return newSubTask;
        } catch (Exception e) {
            System.out.println("Error input.");
        }
        return null;
    }

    ;

    public void updateTask() {
        try {
            System.out.println("Update the task.");
            System.out.println("Input task ID.");
            String idTask = reader.readLine();
            Task taskUpd = allTasks.get(Integer.parseInt(idTask));
            System.out.println("Input task name.");
            String taskName = reader.readLine();
            System.out.println("Input task details.");
            String taskDetails = reader.readLine();
            taskUpd.setTaskName(taskName);
            taskUpd.setTaskDetails(taskDetails);
        } catch (Exception e) {
            System.out.println("Error input.");
        }
    }

    public void updateEpic() {
        try {
            System.out.println("Update the epic.");
            System.out.println("Input epic ID.");
            String idEpic = reader.readLine();
            Task taskUpd = allTasks.get(Integer.parseInt(idEpic));
            System.out.println("Input task name.");
            String taskName = reader.readLine();
            System.out.println("Input task details.");
            String taskDetails = reader.readLine();
            taskUpd.setTaskName(taskName);
            taskUpd.setTaskDetails(taskDetails);
        } catch (Exception e) {
            System.out.println("Error input.");
        }
    }

    public void updateSubTask() {
        try {
            System.out.println("Update the subtask.");
            System.out.println("Input epic ID.");
            String idEpic = reader.readLine();
            System.out.println("Input subtask ID.");
            String idSubtask = reader.readLine();
            System.out.println("Input task name.");
            String taskName = reader.readLine();
            System.out.println("Input task details.");
            String taskDetails = reader.readLine();
            Epic updEpic = (Epic) allTasks.get(Integer.parseInt(idEpic));
            SubTask updSubTask = (SubTask) updEpic.getEpicSubtasks().get(Integer.parseInt(idSubtask));
            updSubTask.setConsistIn(updEpic.getTaskId());
            updSubTask.setTaskName(taskName);
            updSubTask.setTaskDetails(taskDetails);
        } catch (Exception e) {
            System.out.println("Error input.");
        }
    }


    public void createUpdateMenu() {
        System.out.println(" Pls 1 for - Create the Task.");
        System.out.println(" Pls 2 for - Create the Epic.");
        System.out.println(" Pls 3 for - Create the SubTask in some Epic.");

        System.out.println(" Pls 5 for - Update the Task.");
        System.out.println(" Pls 6 for - Update the Epic.");
        System.out.println(" Pls 7 for - Update the SubTask in some Epic.");

        System.out.println(" Pls 9 for - Main menu.");
    }


    public void updateStatus(int id, Status status) {
        if (allTasks.containsKey(id) && !(allTasks.get(id) instanceof Epic)) {
            allTasks.get(id).setCurrentStatus(status);
        } else if (subEpic.containsKey(id)) {
            Epic needEpic = (Epic) allTasks.get(subEpic.get(id));
            needEpic.getEpicSubtasks().get(id).setCurrentStatus(status);
            needEpic.analysisStatus();
        }
    }


}
