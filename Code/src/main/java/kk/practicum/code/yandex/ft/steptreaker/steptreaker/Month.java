package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Month {

    private int daysTotal;

    HashMap<Integer, ArrayList<Integer>> stepsPerDays = new HashMap<>();

    public Month(int daysTotal) {
        this.daysTotal = daysTotal;
        for (int i = 1; i < daysTotal + 1; i++) {
            stepsPerDays.put(i, new ArrayList<>(Arrays.asList(0)));
        }
    }

    public HashMap<Integer, ArrayList<Integer>> getStepsPerDays() {
        return stepsPerDays;
    }

    @Override
    public String toString() {
        return "Month{" +
                "daysTotal=" + daysTotal +
                ", stepsPerDays=" + stepsPerDays +
                '}';
    }
}
