package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.data.DataTaskTracker;
import kk.practicum.code.yandex.ft.tasktracker.options.CreateUpdateOptions;
import kk.practicum.code.yandex.ft.tasktracker.options.DeleteOptions;
import kk.practicum.code.yandex.ft.tasktracker.options.InformationOptions;
import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Status;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


public class InMemoryTaskManager implements TaskManager {

    HashMap<Integer, Task> allTasks = new HashMap<>();

    HashMap<Integer, Integer> subEpic = new HashMap<>();

    TreeSet<Task> sortArrTasks = new TreeSet<>(Comparator.comparing(Task:: getStartTime).reversed());

    HistoryManager history;

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    InformationOptions informator;
    CreateUpdateOptions creator;
    DeleteOptions deleter;

    public InMemoryTaskManager(HistoryManager history) {
        this.history = history;
        memoryGenerator();
        this.informator = new InformationOptions(allTasks, subEpic, history);
        this.creator = new CreateUpdateOptions(allTasks, subEpic, history);
        this.deleter = new DeleteOptions(allTasks, subEpic, history);
//        mainMenu();
        sortTasks();
    }

    public TaskManager runner() {
        mainMenu();
        return null;
    }

    public void memoryGenerator() {

        DataTaskTracker data = new DataTaskTracker();
        allTasks = data.arrTasks;
        subEpic = data.arrSubEpic;
    }

    public void sortTasks() {
        for (Task value : allTasks.values()) {
            if (value instanceof Epic) {
                sortArrTasks.add(value);
                HashMap<Integer,Task> subTasks = ((Epic) value).getEpicSubtasks();
                for (Task task : subTasks.values()) {
                    sortArrTasks.add(task);
                }
                sortArrTasks.add(value);
            }

        }
    }

    public void mainMenu() {

        System.out.println("Welcome to Task-tracker.");
        for (; ; ) {
            try {
                System.out.println("---------------");
                informator.initMenu();
                System.out.println("---------------");
                String choiceStr = reader.readLine();
                switch (Integer.parseInt(choiceStr)) {
                    case (0):
                        System.out.println(sortArrTasks);
                        break;
                    case (15):
                        System.out.println(subEpic);
                        break;
                    case (1):
                        showTasks();
                        break;
                    case (2):
                        detailInformation();
                        break;
                    case (3):
                        deleteOptions(history);
                        break;
                    case (4):
                        createdUpdatedOptions(allTasks, history, reader);
                        break;
                    case (8):
                        System.out.println(history.getHistory());
                        break;
                    case (9):
                        System.out.println("Exit from Task-tracker");
                        exit();
                        throw new IOException("Exit");
                }
            } catch (Exception e) {
                break;
            }
        }
    }


    @Override
    public void showTasks() {
        informator.showTasks();
    }

    @Override
    public void detailInformation() {
        informator.detailInformation();
    }

    @Override
    public HashMap<Integer, Task> getAllTasks() {
        return allTasks;
    }

    @Override
    public Task getTask(int taskID) {
        return allTasks.get(taskID);
    }

    @Override
    public Epic getEpic(int epicID) {
        return (Epic) allTasks.get(epicID);
    }

    @Override
    public SubTask getSubTask(int subTaskID) {
        return (SubTask) allTasks.get(subEpic.get(subTaskID));
    }


    public void deleteOptions(HistoryManager history) {
        try {
            System.out.println("---------------");
            deleter.deleteMenu();
            System.out.println("---------------");
            String deleteStr = reader.readLine();
            switch (Integer.parseInt(deleteStr)) {

                case (1):
                    clearTracker();
                    break;
                case (2):
                    deleteAllTasks();
                    break;
                case (3):
                    deleteAllEpics();
                    break;
                case (4):
                    deleteSubTask();
                    break;
                case (9):
                    System.out.println("Exit from Delete menu.");
                    mainMenu();
                    break;
            }
        } catch (Exception e) {
            System.out.println("Unpredictable input data.");
            mainMenu();
        }
    }


    @Override
    public void deleteTaskOrEpic() {
        deleter.deleteTaskOrEpic();
    }

    @Override
    public void clearTracker() {
        deleter.clearTaskTracker();
    }

    @Override
    public void deleteAllEpics() {
        deleter.deleteAllEpics();
    }

    @Override
    public void deleteAllTasks() {
        deleter.deleteAllTasks();
    }

    @Override
    public void deleteSubTask() {
        try {
            deleter.deleteIDMenu();
            int dellChoice = Integer.parseInt(reader.readLine());
            switch (dellChoice) {
                case (1), (2):
                    deleteTaskOrEpic();
                    break;
                case (3):
                    System.out.println("Input subtask ID.");
                    int subtaskId = Integer.parseInt(reader.readLine());
                    Epic epicUpd = (Epic) allTasks.get(subEpic.get(subtaskId));
                    epicUpd.getEpicSubtasks().remove(subtaskId);
                    subEpic.remove(subtaskId);
                    mainMenu();
                    break;
                case (9):
                    System.out.println("To delete menu.");
                    deleteSubTask();
            }

        } catch (Exception e) {
            System.out.println("Unpredictable ID in input.");
        }
    }


    public void createdUpdatedOptions(HashMap<Integer, Task> allTasks, HistoryManager history, BufferedReader reader) {
        try {
            System.out.println("---------------");
            creator.createUpdateMenu();
            System.out.println("---------------");
            String createStr = reader.readLine();
            switch (Integer.parseInt(createStr)) {
                case (1):
                    Task task = creator.createTaskPreparation();
                    createTask(task);
                    break;
                case (2):
                    Epic epic = creator.createEpikPreparation();
                    createEpic(epic);
                    break;
                case (3):
                    SubTask subTask = creator.createSubtaskPreparation();
                    createSubTask(subTask);
                    break;
                case (4):
                    updateTask();
                    break;
                case (5):
                    updateEpic();
                    break;
                case (6):
                    updateSubTask();
                    break;
                case (9):
                    System.out.println("Exit from Create and Update menu.");
                    mainMenu();
                    break;

            }
        } catch (Exception e) {
            System.out.println("Unpredictable input data.");
        }
    }

    @Override
    public void exit() {

    }

    @Override
    public void createTask(Task task) {
        creator.createTask(task);
    }

    @Override
    public void createEpic(Epic epic) {
        creator.createEpic(epic);
    }

    @Override
    public void createSubTask(SubTask subTask) {creator.createSubTask(subTask);}

    @Override
    public void updateTask() {
        creator.updateTask();
    }

    @Override
    public void updateEpic() {
        creator.updateEpic();
    }

    @Override
    public void updateSubTask() {
        creator.updateSubTask();
    }

    @Override
    public void updateStatus(int id, Status status) {creator.updateStatus(id,status);}
}



