package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

public interface TrackerInfo {


    void changeDailySteps();
    int showMonth();
    void createYear(Integer year);
    void addSteps();
    void addStepsTest(int month, int day, int steps);


}
