package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.structure.Task;

import java.util.List;

public interface HistoryManager {

    void add(Task task);
    void remote(int id );
    List<Task> getHistory();
    void clear();

}
