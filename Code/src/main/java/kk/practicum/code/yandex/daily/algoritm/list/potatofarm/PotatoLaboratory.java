package kk.practicum.code.yandex.daily.algoritm.list.potatofarm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PotatoLaboratory {

    public static void main(String[] args) {
        List<Potato> potatoes = List.of(
                new Potato(210, 30, 30),
                new Potato(120, 35, 31),
                new Potato(130, 40, 35),
                new Potato(240, 28, 44),
                new Potato(150, 33, 23),
                new Potato(60, 35, 33),
                new Potato(70, 38, 41)
        );

        List<Potato> fourUnderExperiment = findPotatoesForExperiment(potatoes);

        System.out.println("Картофелины для эксперимента: - Random \n" + fourUnderExperiment );

    }

    private static List<Potato> findPotatoesForExperiment(List<Potato> potatoes) {
        List<Potato> potatoAdd = new ArrayList<>(potatoes);
        List<Potato> potatoSelection = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            Potato potatoMax = Collections.max(potatoAdd);
            Potato potatoMin = Collections.min(potatoAdd);
            potatoSelection.add(potatoMax);
            potatoSelection.add(potatoMin);
            potatoAdd.remove(potatoMax);
            potatoAdd.remove(potatoMin);
        }
        potatoSelection.sort(Potato::compareTo);
        List<Potato> potatoSelectionReversed = potatoSelection.reversed();
        return potatoSelectionReversed;
    }
}