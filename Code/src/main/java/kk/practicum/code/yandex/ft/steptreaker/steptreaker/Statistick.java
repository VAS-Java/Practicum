package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

import java.time.LocalDate;
import java.util.*;

public class Statistick {

    StepTracker trackerRun;
    int numberMonth = 0;

    String stepsNote = "";
    int stepsPerMonth = 0;
    int stepsMax = 0;
    int bestDaySteps = 0;
    int counterForBestSteps = 0;


    public Statistick(StepTracker trackerRun, Integer numberMonth) {
        this.trackerRun = trackerRun;
        this.numberMonth = numberMonth;

    }


    public void infoStatistic() {

        HashMap<Integer, Integer> updAnalysingMonth = new HashMap<>();
        Month analysingMonth = trackerRun.getYears().get(LocalDate.now().getYear()).getDataYear().get(numberMonth);

        for (int i = 0; i <= analysingMonth.stepsPerDays.size() - 1; i++) {
            int stepsPerDay = 0;
            for (int j = 0; j < analysingMonth.stepsPerDays.get(i + 1).size(); j++) {
                stepsPerDay += analysingMonth.stepsPerDays.get(i + 1).get(j);
            }
            stepsPerMonth += stepsPerDay;

            if (stepsPerDay >= trackerRun.getStepsDaily()) {
                counterForBestSteps += 1;
            } else {
                if (counterForBestSteps > bestDaySteps) {
                    bestDaySteps = counterForBestSteps;
                    counterForBestSteps = 0;
                } else {
                    counterForBestSteps = 0;
                }
            }
            updAnalysingMonth.put(i, stepsPerDay);
            stepsMax = Collections.max(updAnalysingMonth.values());
        }
    }

    protected void printStatistic() {
        infoStatistic();
        System.out.printf("%s".formatted(Months.values()[numberMonth]));
        System.out.println(stepsNote);
        System.out.println("Среднее колличество шагов - " + stepsPerMonth / 30);
        System.out.println("Максимальное колличество шагов -  " + stepsMax);
        System.out.println("Пройденная дистанция - " + stepsPerMonth * 0.00075 + " Км.");
        System.out.println("Сожжено " + stepsPerMonth / 20 + " Ккал.");
        System.out.println(bestDaySteps);
    }

}


