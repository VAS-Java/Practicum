package kk.practicum.code.yandex.ft.tasktracker.structure;

import kk.practicum.code.yandex.ft.tasktracker.managers.HistoryManager;

import java.util.HashMap;

public class SubTask extends Task {

    int consistIn;

    public SubTask(String taskName, String taskDetails) {
        super(taskName, taskDetails);

    }

    @Override
    public void showDetails(HashMap<Integer, Task> allTasks,HashMap<Integer, Integer> subEpic, HistoryManager history) {
        super.showDetails(allTasks, subEpic, history);
    }

    public int getConsistIn() {
        return consistIn;
    }

    public void setConsistIn(int consistIn) {
        this.consistIn = consistIn;
    }


}
