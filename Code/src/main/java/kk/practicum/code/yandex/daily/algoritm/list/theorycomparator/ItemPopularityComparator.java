package kk.practicum.code.yandex.daily.algoritm.list.theorycomparator;

import java.util.Comparator;

public class ItemPopularityComparator implements Comparator<Item> {

    @Override
    public int compare(Item i1, Item i2) {
        return i1.popularity - i2.popularity;
    }



}
