package kk.practicum.code.yandex.daily.algoritm.list.potatofarm;

import java.util.Objects;

public class Potato implements Comparable<Potato> {

    private static int id = 15000;
    private final int potatoId = id;
    public final int weight;
    public final int length;
    public final int girth;

    public Potato( int weight, int length, int girth) {
        ++id;
        this.weight = weight;
        this.length = length;
        this.girth = girth;
    }
    public int calculateAlpha() {
        return (int) (this.weight * 0.5 + this.length * 0.6 + this.girth * 0.8  * 10000);
    }

    @Override
    public int compareTo(Potato o) {
        return this.calculateAlpha() - o.calculateAlpha();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Potato potato = (Potato) o;
        return potatoId == potato.potatoId && weight == potato.weight && length == potato.length && girth == potato.girth;
    }

    @Override
    public int hashCode() {
        return Objects.hash(potatoId, weight, length, girth);
    }

    @Override
    public String toString() {
        return "Potato{" +
                ", potatoId=" + potatoId +
                "girth=" + girth +
                ", weight=" + weight +
                ", length=" + length +
                '}';
    }
}
