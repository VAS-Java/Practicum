package kk.practicum.code.yandex.ft.steptreaker.steptreaker;

import java.util.HashMap;

public class Year {

    private boolean leapYear;

    HashMap<Integer,Month> dataYear = new HashMap();

    public Year(Integer year) {
        this.leapYear = leapYearCheced(year);

        for (int i = 0; i < Months.values().length; i++) {
            Month newMonth = new Month(daysPerMonth((Months.values()[i])));
            dataYear.put(i+1,newMonth);
        }

    }

    public HashMap<Integer, Month> getDataYear() {
        return dataYear;
    }

    public int daysPerMonth(Months month) {
        switch (month) {
            case JANUARY:
            case MARCH:
            case MAY:
            case JULY:
            case AUGUST:
            case OCTOBER:
            case DECEMBER:
                return 31;
            case FEBRUARY:
                return leapYear ? 29: 28;
            default:
                return 30;
        }

    }


    public boolean leapYearCheced(Integer year) {
        return (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) ? true : false;
    }

    @Override
    public String toString() {
        return "Year{" +
                "dataYear=" + dataYear +
                '}';
    }
}
