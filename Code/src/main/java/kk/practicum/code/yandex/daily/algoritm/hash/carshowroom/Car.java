package kk.practicum.code.yandex.daily.algoritm.hash.carshowroom;


import java.util.Objects;

public class Car
//        implements Comparable<Car>
{


    private final String model;
    private final Integer priceInRubles;

    public Car(String model, Integer priceInRubles) {
        this.model = model;
        this.priceInRubles = priceInRubles;
    }

//    public String getModel() {
//        return model;
//    }

    public Integer getPriceInRubles() {
        return priceInRubles;
    }

//    @Override
//    public int compareTo(Car o) {
//        return this.priceInRubles - o.priceInRubles;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(model);
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", priceInRubles=" + priceInRubles +
                '}';
    }
}
