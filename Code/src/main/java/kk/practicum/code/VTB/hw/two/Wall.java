package kk.practicum.code.VTB.hw.two;

class Wall extends Obstacle{

    protected int height;

    public Wall(int height) {
        this.height = height;
    }

    public boolean activ(Activities activities) {
        if (activities.jump(height)) {
            return true;
        }else {
            return false;
        }
    }
}
