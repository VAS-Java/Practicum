package kk.practicum.code.VTB.hw.two;

public class Human implements Activities {

    String name;
    int runDistance;
    int jumpHeight;
    boolean actual = true;

    public Human(String name, int runDistance, int jumpHeight) {
        this.name = name;
        this.runDistance = runDistance;
        this.jumpHeight = jumpHeight;
    }


    public boolean run(int distanceObstacle) {
        if (distanceObstacle <= this.runDistance) {
            System.out.println(this.name + " run " + distanceObstacle + " m. ");
            return true;
        } else {
            System.out.println(this.name + " drop out of the race.");
            return false;
        }
    }

    public boolean jump(int heightObstacle) {
        if (heightObstacle <= this.jumpHeight) {
            System.out.println(this.name + " jump " + heightObstacle + " sm. ");
            return true;
        } else {
            System.out.println(this.name + " drop out of the race.");
            return false;

        }
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                '}';
    }
}
