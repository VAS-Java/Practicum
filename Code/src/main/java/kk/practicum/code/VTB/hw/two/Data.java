package kk.practicum.code.VTB.hw.two;

import java.util.ArrayList;

public class Data {


    ArrayList<Obstacle> obstacles = new ArrayList<>();
    ArrayList<Activities> participants = new ArrayList<>();

    Data() {
        Obstacle firstTreadmill = new Treadmill( 200);
        obstacles.add(firstTreadmill);
        Obstacle firstWall = new Wall( 40);
        obstacles.add(firstWall);
        Obstacle secondTreadmill = new Treadmill( 400);
        obstacles.add(secondTreadmill);
        Obstacle secondWall = new Wall( 50);
        obstacles.add(secondWall);
        Obstacle thirdTreadmill = new Treadmill( 600);
        obstacles.add(thirdTreadmill);
        Obstacle thirdWall = new Wall( 60);
        obstacles.add(thirdWall);
        Obstacle fourWall = new Wall( 70 );
        obstacles.add(fourWall);
        Obstacle fourTreadmill = new Treadmill( 800);
        obstacles.add(fourTreadmill);


        Human maleEuropean = new Human("Mark", 1500, 70);
        Human femaleEuropean = new Human("Zelda", 1100, 60);
        Cat standartCat = new Cat("Kote", 200, 100);
        Robot projectHelper = new Robot("R2-D2",500,50);
        participants.add(maleEuropean);
        participants.add(femaleEuropean);
        participants.add(standartCat);
        participants.add(projectHelper);



    }


}
