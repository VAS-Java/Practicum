package kk.practicum.code.VTB.hw.four;

public class FruitStock {


    public static void generateStock() {
        Box<Apple> appleBox60 = new Box<>();
        Box<Apple> appleBox20 = new Box<>();
        Box<Orange> orangeBox60 = new Box<>();
        Box<Orange> orangeBox20 = new Box<>();
        System.out.println(" ");
        System.out.println("Box's");
        System.out.println(" ");
        System.out.println("Weight apple meddle box - " + appleBox60.getWeight() + " kg.");
        System.out.println("Weight apple lite box - " + appleBox20.getWeight() + " kg.");
        System.out.println("Weight orange meddle box - " + orangeBox60.getWeight() + " kg.");
        System.out.println("Weight orange lite box - " + orangeBox20.getWeight() + " kg.");
        System.out.println(" ");
        System.out.println("Add fruit's to box's. ");
        System.out.println(" ");
        for (int i = 0; i < 40; i++) {
            if (i < 10) {
                orangeBox20.addFruit(new Orange());
            }
            if (i < 15) {
                appleBox20.addFruit(new Apple());
            }
            appleBox60.addFruit(new Apple());
            orangeBox60.addFruit(new Orange());
        }

        System.out.println("Weight apple meddle box - " + appleBox60.getWeight() + " kg.");
        System.out.println("Weight apple lite box - " + appleBox20.getWeight() + " kg.");
        System.out.println("Weight orange meddle box - " + orangeBox60.getWeight() + " kg.");
        System.out.println("Weight orange lite box - " + orangeBox20.getWeight() + " kg.");
        System.out.println("Compare lite box - " + appleBox20.Compare(orangeBox20) + " .");
        System.out.println("Compare middle box - " + appleBox60.Compare(orangeBox60) + " .");
        System.out.println(" ");
        System.out.println("Pour from box to box. ");
        System.out.println(" ");
        appleBox60.addBoxFruit(appleBox20);
        System.out.println("Weight apple meddle box - " + appleBox60.getWeight() + " kg.");
        System.out.println("Weight apple lite box - " + appleBox20.getWeight() + " kg.");
        System.out.println("Compare lite box - " + appleBox20.Compare(orangeBox20) + " .");
        orangeBox60.addBoxFruit(orangeBox20);
        System.out.println("Weight orange meddle box - " + orangeBox60.getWeight() + " kg.");
        System.out.println("Weight orange lite box - " + orangeBox20.getWeight() + " kg.");
        System.out.println("Compare lite box - " + appleBox20.Compare(orangeBox20) + " .");


    }


}
