package kk.practicum.code.VTB.hw.three;

public class MyArrayDataException extends HandException {

    private int row;
    public  int column;
    private String [] value;

    public MyArrayDataException(int row, int column, String [] value) {
        super("Incorect value in - " + row + " - " + column);
        this.row = row;
        this.column = column;
        this.value = value;
    }
}
