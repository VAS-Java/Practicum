package kk.practicum.code.VTB.hw.one;

public class Dog extends Animal {
    public static int count = 0;

    String type = "Dog";
    protected String name;
    protected int age;

    public Dog(String aria, int runDistance, boolean canSwim, int swimDistance, String name, int age) {
        super(aria, runDistance, canSwim, swimDistance);
        this.name = name;
        this.age = age;
        count++;
    }

    @Override
    void sound() {
        super.sound();
        System.out.println("Гавгав.");
    }
}

