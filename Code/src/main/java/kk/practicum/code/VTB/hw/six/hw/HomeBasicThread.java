package kk.practicum.code.VTB.hw.six.hw;

public class HomeBasicThread {

    static final int SIZE = 10_000_000;
    static final int HALF = SIZE / 2;
    float[] arr = new float[SIZE];

    float[] arrOne = new float[HALF];
    float[] arrTwo = new float[HALF];

    public static void main(String[] args) {
        HomeBasicThread hh = new HomeBasicThread();
        hh.creator();
        hh.creatorThread();
    }

    public void creator() {
        System.out.println("Run main thread");
        for (int i = 0; i < 10_000_000; i++) {
            arr[i] = 1;
        }

        Long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10_000_000; i++) {
            arr[i] = (float) (arr[i] * Math.sin(0.2f + (float) i / 5) * Math.cos(0.2f + (float) i / 5) * Math.cos(0.4f + (float) i / 2));
        }


        Long endTime = System.currentTimeMillis();
        System.out.println("Stop main tread");
        System.out.println(endTime - startTime);
    }

    public void creatorThread() {
        for (int i = 0; i < 10_000_000; i++) {
            arr[i] = 1;
        }

        Long startTime = System.currentTimeMillis();

        System.arraycopy(arr, 0, arrOne, 0, HALF);
        System.arraycopy(arr, HALF, arrTwo, 0, HALF);

        Thread tOne = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Run thread One");
                for (int i = 0; i < arrOne.length; i++) {
                    arrOne[i] = (float) (arrOne[i] * Math.sin(0.2f + (float) i / 5) * Math.cos(0.2f + (float) i / 5) * Math.cos(0.4f + (float) i / 2));
                }
            }
        });

        Thread tTwo = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Run thread Two");
                for (int i = HALF; i < SIZE - 1; i++) {
                    arrTwo[i - HALF] = (float) (arrTwo[i - HALF] * Math.sin(0.2f + (float) i / 5) * Math.cos(0.2f + (float) i / 5) * Math.cos(0.4f + (float) i / 2));
                }
            }
        });


        tOne.start();
        tTwo.start();
        try {
            tOne.join();
            tTwo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stop thread's");
        System.arraycopy(arrOne, 0, arr, 0, HALF);
        System.arraycopy(arrTwo, 0, arr, HALF, HALF);

        Long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime);

    }




}


