package kk.practicum.code.VTB.hw.one;

import java.util.ArrayList;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        System.out.println("VTB");
        Data animals = new Data();



        for (Animal animal : animals.animalArr) {
            if (animal instanceof Cat) {
                Activities.run(450, animal.runDistance, ((Cat) animal).type );
                Activities.swim(35, animal.canSwim, animal.swimDistance, ((Cat) animal).type);

            } else if (animal instanceof Dog) {
                Activities.run(450, animal.runDistance, ((Dog) animal).type );
                Activities.swim(35, animal.canSwim, animal.swimDistance, ((Dog) animal).type);

            } else if (animal instanceof Tiger) {
                Activities.run(450, animal.runDistance, ((Tiger) animal).type );
                Activities.swim(35, animal.canSwim, animal.swimDistance, ((Tiger) animal).type);
            }
            animal.sound();
        }
        System.out.println("Всего - " + Animal.count + " элементов в массиве животных.");

    }


}
