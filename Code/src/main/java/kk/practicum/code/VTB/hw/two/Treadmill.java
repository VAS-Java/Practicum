package kk.practicum.code.VTB.hw.two;

class Treadmill extends Obstacle {

    protected int distance;

    public Treadmill(int distance) {
        this.distance = distance;
    }

    @Override
    public boolean activ(Activities activities) {
        if (activities.run(distance)) {
            return true;
        }else {
            return false;
        }
    }
}
