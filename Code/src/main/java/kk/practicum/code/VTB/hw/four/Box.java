package kk.practicum.code.VTB.hw.four;

import java.util.ArrayList;

public class Box<T extends Fruit> {

    ArrayList<T> boxFruit = new ArrayList<>();
    float boxWeight = 0f;


    public float getWeight() {
        boxWeight = 0f;
        for (T item : boxFruit) {

            boxWeight += item.weight;
        }
        return boxWeight;
    }

    public void clearBox() {
        this.boxFruit.clear();
    }


    public boolean Compare(Box compareingBox) {

//        if (compareingBox.getWeight() == getWeight()) {
//            return true;
//        }
//        return false;

        return Math.abs(this.getWeight() - compareingBox.getWeight()) < 0.0001;
    }

    public void addFruit(T fruit) {
        this.boxFruit.add(fruit);
    }

    public void addBoxFruit(Box<T> boxAddFruit) {
        this.boxFruit.addAll(boxAddFruit.boxFruit);
        boxAddFruit.clearBox();
    }

    @Override
    public String toString() {
        return "Box{" +
                "boxFruit=" + boxFruit +
                ", boxWeight=" + boxWeight +
                '}';
    }

}
