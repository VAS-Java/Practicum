package kk.practicum.code.VTB.hw.five;

import java.util.*;

public class UniqueCar {


    public static void counterCar() {
        String [] testArr = new String[] {"Fiat", "Skoda", "VAG", "BMV", "Ford", "Skoda", "Toyota",
                "Nisan", "BMV", "VAG", "LADA", "HONDA", "Renault", "Nisan", "Renault", "Seat", "VAG", "Skoda"};
        ArrayList<String> testLst = new ArrayList<>(Arrays.asList(testArr));
        Set<String> testSet = new HashSet<>(testLst);


        System.out.println(testLst.size() + " Full car list - " + testLst);
        System.out.println(testSet.size() + " Unique car list - " + testSet);

        for (String model : testSet) {
            System.out.println(model + " - " + Collections.frequency(testLst, model));
        }


//        HashMap<String, Integer> testMap = new HashMap<>();
//        for (String itemSet : testSet) {
//            int counter = 0;
//            for (String itemLst : testLst) {
//                if (Objects.equals(itemSet, itemLst)) {
//                    counter += 1;
//                }
//            }
//            testMap.put(itemSet, counter);
//        }
//        for (Map.Entry<String, Integer> item : testMap.entrySet()) {
//            System.out.println(item.getKey() + " - in Array - " + item.getValue());
//
//        }

    }



}
