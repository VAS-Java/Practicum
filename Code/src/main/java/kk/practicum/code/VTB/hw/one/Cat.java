package kk.practicum.code.VTB.hw.one;

public class Cat extends Animal {
    public static int count = 0;

    protected String type = "Cat";
    protected String name;
    protected int age;


    public Cat(String aria, int runDistance, boolean canSwim, int swimDistance, String name, int age) {
        super(aria, runDistance, canSwim, swimDistance);
        this.name = name;
        this.age = age;
        count++;
    }

    @Override
    void sound() {
        super.sound();
        System.out.println("Мяууу.");
    }
}
