package kk.practicum.code.VTB.hw.one;

public abstract class Animal {
    public static int count = 0;

    protected String aria;

    protected int runDistance;

    protected boolean canSwim;
    protected int swimDistance;

    public Animal(String aria, int runDistance, boolean canSwim, int swimDistance) {
        this.aria = aria;
        this.runDistance = runDistance;
        this.canSwim = canSwim;
        this.swimDistance = swimDistance;
        count++;
    }

    void sound () {}

}
