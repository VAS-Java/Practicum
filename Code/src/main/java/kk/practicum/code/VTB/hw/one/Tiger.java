package kk.practicum.code.VTB.hw.one;

public class Tiger extends Animal {
    public static int count = 0;

    String type = "Tiger";
    protected String name;
    protected int age;

    public Tiger(String aria, int runDistance, boolean canSwim, int swimDistance, String name, int age) {
        super(aria, runDistance, canSwim, swimDistance);
        this.name = name;
        this.age = age;
        count++;
    }


    @Override
    void sound() {
        super.sound();
        System.out.println("Ррррррр.");
    }
}
