package kk.practicum.code.VTB.hw.one;

public class Activities {


    static void run(int distance, int runDistance, String animalType) {
        if (distance <= runDistance) {
            System.out.println(animalType + " running - " + distance + " m.");
        } else {
            System.out.println(animalType + " can't run - " + distance + " m.");
        }
    }


    static void swim(int distance, boolean canSwim, int runDistance, String animalType) {
        if (!canSwim) {
            System.out.println("The " + animalType + " can't swim.");
            return;
        }
        if (distance <= runDistance) {
            System.out.println(animalType + " swimming - " + distance + " m.");
        } else {
            System.out.println(animalType + " can't swim - " + distance + " m.");
        }
    }


}
