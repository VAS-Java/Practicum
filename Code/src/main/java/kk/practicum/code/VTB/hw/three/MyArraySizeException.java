package kk.practicum.code.VTB.hw.three;

import java.util.Objects;

public class MyArraySizeException extends HandException{
    private int sizeObj;

    public MyArraySizeException(int sizeObj) {
        super("Invalid matrix size.");
        this.sizeObj = sizeObj;
    }
}
