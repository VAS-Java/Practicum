package leetcode.simlpe;

import java.util.List;

public class TwoSum {

    public static void main(String[] args) {
        twoSum(new int[]{3,2,4}, 6);
        twoSum(new int[]{3, 6, 9, 19}, 15);

    }


    public static int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if ((nums[i] + nums[j] == target) && !(i == j)){
                    System.out.println(i + " " + j);
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }

}
