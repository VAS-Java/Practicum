package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

public class InMemoryHistoryManagerTest {

    HistoryManager historyManager = new InMemoryHistoryManager();

    Task newJava = new SubTask("JAVA", "TEST");
    Task newPython = new SubTask("PYTHON", "TEST");
    Task newJavaScript = new SubTask("JavaScript", "TEST");
    Task newHTML = new SubTask("HTML", "TEST");
    Task newCSS = new SubTask("CSS", "TEST");



    @Test
    public void testClearHistory() {
        Assert.assertEquals(historyManager.getHistory().size(), 0);
    }


    @Test
    public void testAddHistory() {
        historyManager.add(newJava);
        Assert.assertNotNull(historyManager.getHistory());
        Assert.assertEquals(historyManager.getHistory().size(), 1);
        historyManager.add(newJava);
        Assert.assertEquals(historyManager.getHistory().size(), 1);
        historyManager.add(newPython);
        historyManager.add(newJavaScript);
        historyManager.add(newHTML);
        historyManager.add(newCSS);
        Assert.assertEquals(historyManager.getHistory().size(), 5);

    }

    @Test
    public void clearTestHistory() {
        historyManager.add(newJava);
        historyManager.add(newPython);
        historyManager.add(newJavaScript);
        historyManager.add(newHTML);
        historyManager.add(newCSS);
        Assert.assertEquals(historyManager.getHistory().size(), 5);
        historyManager.clear();
        Assert.assertEquals(historyManager.getHistory().size(), 0);

    }
    @Test
    public void removeTestHistory() {
        historyManager.remote(newJava.getTaskId());
        historyManager.add(newJava);
        historyManager.add(newPython);
        historyManager.add(newJavaScript);
        historyManager.add(newHTML);
        historyManager.add(newCSS);
        Assert.assertEquals(historyManager.getHistory().size(), 5);
        historyManager.remote(newJava.getTaskId());
        Assert.assertEquals(historyManager.getHistory().size(), 4);
        historyManager.remote(newCSS.getTaskId());
        Assert.assertEquals(historyManager.getHistory().size(), 3);
        historyManager.remote(newJavaScript.getTaskId());
        Assert.assertEquals(historyManager.getHistory().size(), 2);


    }


}