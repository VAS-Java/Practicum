package kk.practicum.code.yandex.ft.tasktracker.managers;


import org.junit.Before;



public class InMemoryTaskManagerTest extends TaskManagerTest<InMemoryTaskManager> {


    @Before
    @Override
    public void initManager() {
        manager = new InMemoryTaskManager(Managers.getDefaultHistory());
    }

}