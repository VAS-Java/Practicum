package kk.practicum.code.yandex.ft.tasktracker.managers;

import kk.practicum.code.yandex.ft.tasktracker.structure.Epic;
import kk.practicum.code.yandex.ft.tasktracker.structure.Status;
import kk.practicum.code.yandex.ft.tasktracker.structure.SubTask;
import kk.practicum.code.yandex.ft.tasktracker.structure.Task;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public abstract class TaskManagerTest<T extends TaskManager> {
    protected T manager;


    @Before
    public abstract void initManager();


    @Test
    public void testAddTask() {
        Task newTask = new SubTask("Fix", "Dishwasher");
        manager.createTask(newTask);
        Task savedTask = manager.getTask(newTask.getTaskId());
        Assert.assertNotNull("SavedTask not found.", savedTask);
        Assert.assertEquals("Task's not Equals.", newTask, savedTask);
        Assert.assertNotNull("We don't have task.", newTask);
        Assert.assertEquals("Wrong length.", 1, manager.getAllTasks().size());
        Assert.assertEquals("Task's not Equals.", newTask, manager.getTask(savedTask.getTaskId()));

        manager.updateStatus(savedTask.getTaskId(), Status.IN_PROGRESS);
        Assert.assertEquals(savedTask.getCurrentStatus(), Status.IN_PROGRESS);
        Assert.assertEquals(newTask.getCurrentStatus(), Status.IN_PROGRESS);
        manager.updateStatus(savedTask.getTaskId(), Status.DONE);
        Assert.assertEquals(savedTask.getCurrentStatus(), Status.DONE);
        Assert.assertEquals(newTask.getCurrentStatus(), Status.DONE);

    }

    @Test
    public void testAddEpic() {
        Task newEpic = new Epic("Fix", "Dishwasher");
        manager.createTask(newEpic);
        Task savedEpic = manager.getTask(newEpic.getTaskId());
        Assert.assertNotNull("SavedTask not found.", savedEpic);
        Assert.assertEquals("Task's not Equals.", newEpic, savedEpic);
        Assert.assertNotNull("We don't have task.", newEpic);
        Assert.assertEquals("Wrong length.", 1, manager.getAllTasks().size());
        Assert.assertEquals("Task's not Equals.", newEpic, manager.getTask(savedEpic.getTaskId()));

        Assert.assertEquals(newEpic.getCurrentStatus(), Status.NEW);
        Assert.assertEquals(savedEpic.getCurrentStatus(), Status.NEW);

        SubTask newSubTask = new SubTask("Repair", "Pipes");
        newSubTask.setConsistIn(newEpic.getTaskId());
        manager.createSubTask(newSubTask);
        SubTask addSubTask = new SubTask("Repair", "Electrical");
        addSubTask.setConsistIn(newEpic.getTaskId());
        manager.createSubTask(addSubTask);

        Assert.assertEquals(newEpic.getCurrentStatus(), Status.NEW);
        Assert.assertEquals(savedEpic.getCurrentStatus(), Status.NEW);
        manager.updateStatus(addSubTask.getTaskId(),Status.IN_PROGRESS);
        Assert.assertEquals(newEpic.getCurrentStatus(), Status.IN_PROGRESS);
        Assert.assertEquals(savedEpic.getCurrentStatus(), Status.IN_PROGRESS);
        manager.updateStatus(addSubTask.getTaskId(),Status.DONE);
        manager.updateStatus(newSubTask.getTaskId(),Status.DONE);
        Assert.assertEquals(newEpic.getCurrentStatus(), Status.DONE);
        Assert.assertEquals(savedEpic.getCurrentStatus(), Status.DONE);


    }

    @Test
    public void testAddSubTask() {

        Epic newEpic = new Epic("Fix", "Dishwasher");
        manager.createEpic(newEpic);

        SubTask newSubTask = new SubTask("Repair", "Pipes");
        newSubTask.setConsistIn(newEpic.getTaskId());
        manager.createSubTask(newSubTask);

        Task savedEpic = manager.getTask(newEpic.getTaskId());
        Task savedSubTask = manager.getEpic(newEpic.getTaskId()).getEpicSubtasks().get(newSubTask.getTaskId());


        Assert.assertNotNull("SavedTask not found.", savedEpic);
        Assert.assertEquals("Task's not Equals.", newEpic, savedEpic);
        Assert.assertNotNull("We don't have task.", newEpic);
        Assert.assertEquals("Wrong length.", 1, manager.getAllTasks().size());
        Assert.assertEquals("Task's not Equals.", newEpic, manager.getTask(savedEpic.getTaskId()));

        Assert.assertNotNull("SavedTask not found.", savedSubTask);
        Assert.assertEquals("Task's not Equals.", newSubTask, savedSubTask);
        Assert.assertNotNull("We don't have task.", newSubTask);
        Assert.assertEquals("Wrong length.", 1, manager.getEpic(newEpic.getTaskId()).getEpicSubtasks().size());
        Assert.assertEquals("Task's not Equals.", newSubTask, manager.getEpic(newEpic.getTaskId()).getEpicSubtasks().get(savedSubTask.getTaskId()));

        manager.updateStatus(savedSubTask.getTaskId(), Status.IN_PROGRESS);
        Assert.assertEquals("In progress", newSubTask.getCurrentStatus(), Status.IN_PROGRESS);

        manager.updateStatus(savedSubTask.getTaskId(), Status.DONE);
        Assert.assertEquals("In progress", newSubTask.getCurrentStatus(), Status.DONE);

        Assert.assertNotNull(manager.getAllTasks().get(newSubTask.getConsistIn()));

    }

}






