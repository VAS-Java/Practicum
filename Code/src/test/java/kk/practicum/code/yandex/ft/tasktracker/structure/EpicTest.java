package kk.practicum.code.yandex.ft.tasktracker.structure;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EpicTest {
    Epic epicTest = new Epic("RDC", "ATC");
    Task subtaskTest = new SubTask("ATC", "Radar");
    Task subtaskAddTest = new SubTask("ATC", "Planer");


    @Test
    void epicStatusWithoutSubtaskTest() {
        assertEquals(Status.NEW, epicTest.getCurrentStatus());
    }

    @BeforeEach
    public void beforeEach() {

        epicTest.getEpicSubtasks().put(subtaskTest.getTaskId(), subtaskTest);
        epicTest.getEpicSubtasks().put(subtaskAddTest.getTaskId(), subtaskAddTest);

    }

    @Test
    void epicStatusNEWSubtaskTest() {
        assertEquals(Status.NEW,epicTest.getCurrentStatus());
    }

    @Test
    void epicStatusDONESubtaskTest() {
        epicTest.getEpicSubtasks().get(subtaskTest.getTaskId()).setCurrentStatus(Status.DONE);
        epicTest.getEpicSubtasks().get(subtaskAddTest.getTaskId()).setCurrentStatus(Status.DONE);
        epicTest.analysisStatus();
        assertEquals(Status.DONE,epicTest.getCurrentStatus());
    }

    @Test
    void epicStatusNEWDONESubtaskTest() {
        epicTest.getEpicSubtasks().get(subtaskTest.getTaskId()).setCurrentStatus(Status.DONE);
        epicTest.analysisStatus();
        assertEquals(Status.IN_PROGRESS,epicTest.getCurrentStatus());
    }
    @Test
    void epicStatusInProgress() {
        epicTest.getEpicSubtasks().get(subtaskTest.getTaskId()).setCurrentStatus(Status.IN_PROGRESS);
        epicTest.getEpicSubtasks().get(subtaskAddTest.getTaskId()).setCurrentStatus(Status.IN_PROGRESS);
        epicTest.analysisStatus();
        assertEquals(Status.IN_PROGRESS,epicTest.getCurrentStatus());
    }

}