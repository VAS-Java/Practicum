package com.pet.care.vetgramm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VetgrammApplication {

	public static void main(String[] args) {
		SpringApplication.run(VetgrammApplication.class, args);
	}

}
